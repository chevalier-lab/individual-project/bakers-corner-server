package com.aitekteam.developer.bakercorneradmin.models.items

import java.io.Serializable

data class HomeMenuItem (
    var label: String
): Serializable
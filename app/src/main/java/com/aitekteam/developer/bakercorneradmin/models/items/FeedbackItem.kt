package com.aitekteam.developer.bakercorneradmin.models.items

import com.aitekteam.developer.bakercorneradmin.models.Users

data class FeedbackItem (
    var uid: String = "",
    var description: String = "",
    var user: Users = Users(),
    var timestamp: Long = System.currentTimeMillis()
)
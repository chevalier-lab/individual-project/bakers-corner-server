package com.aitekteam.developer.bakercorneradmin.activities.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.fragments.FeedbackFragment
import com.aitekteam.developer.bakercorneradmin.fragments.HomeFragment
import com.aitekteam.developer.bakercorneradmin.fragments.OurUsersFragment
import com.aitekteam.developer.bakercorneradmin.fragments.TransactionFragment
import com.aitekteam.developer.bakercorneradmin.services.ReceiverService
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private var currentId: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        // Setup Toolbar
        setupToolbar()

        // Setup BottomNavigationBar
        homeMenu.setOnNavigationItemSelectedListener(this.mOnNavigationItemSelectedListener)
        supportFragmentManager.beginTransaction().replace(R.id.homeFrame, HomeFragment()).commit()
    }

    // Setup Toolbar
    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.bakers_corner)
        }
    }

    // Setup BottomNavigationBar
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val fragmentCheck = fun(fragmentId: Int, fragment: Fragment) {
            if (currentId != fragmentId) {
                supportFragmentManager.beginTransaction().replace(R.id.homeFrame, fragment).commit()
            }
        }

        when (item.itemId) {
            R.id.item_home -> fragmentCheck(R.id.item_home, HomeFragment())
            R.id.item_transaction -> fragmentCheck(R.id.item_transaction, TransactionFragment())
            R.id.item_feedback -> fragmentCheck(R.id.item_feedback, FeedbackFragment())
            R.id.item_users -> fragmentCheck(R.id.item_users, OurUsersFragment())
        }

        currentId = item.itemId
        true
    }

    override fun onStop() {
        Log.i("BC-MyService", "Kill activity, sending broadcast")
        val broadcastIntent = Intent()
        broadcastIntent.action = "RestartService"
        broadcastIntent.setClass(this, ReceiverService::class.java)
        sendBroadcast(broadcastIntent)
        super.onStop()
    }
}

package com.aitekteam.developer.bakercorneradmin.activities.product

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.aitekteam.developer.bakercorneradmin.BuildConfig
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.main.MainActivity
import com.aitekteam.developer.bakercorneradmin.models.Product
import com.aitekteam.developer.bakercorneradmin.models.ProductCategory
import com.aitekteam.developer.bakercorneradmin.services.MqttService
import com.aitekteam.developer.bakercorneradmin.utils.*
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_create_product.*
import kotlinx.android.synthetic.main.component_toolbar.*
import org.eclipse.paho.client.mqttv3.MqttMessage
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.*

@Suppress("SpellCheckingInspection")
class CreateProductActivity : AppCompatActivity() {

    // Declare Variable
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var storage: FirebaseStorage
    private lateinit var progressDialog: ProgressDialog
    private var selectedCover: Bitmap? = null
    private var selectedCategory: ProductCategory? = null
    private var mqtt: MqttService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_product)

        // Setup Toolbar
        setupToolbar()

        // init mqtt
        mqtt = MqttService(this)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this@CreateProductActivity)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Authentication
        auth = FirebaseAuth.getInstance()

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        // Setup Storage
        storage = FirebaseStorage.getInstance()

        progressDialog.show()

        // Setup Category Product
        database.child(DB_CATEGORIES).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.childrenCount > 0) {
                    val items: MutableList<ProductCategory> = arrayListOf()
                    val itemsString: MutableList<String> = arrayListOf()
                    for (item: DataSnapshot in p0.children) {
                        item.getValue(ProductCategory::class.java)?.apply {
                            items.add(this)
                            itemsString.add(this.category)
                        }
                    }

                    selectedCategory = items[0]
                    editProductInfoCategory.setAdapter(
                        ArrayAdapter(
                            this@CreateProductActivity,
                            android.R.layout.select_dialog_singlechoice, itemsString
                        )
                    )
                    editProductInfoCategory.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(parent: AdapterView<*>?) {}

                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                selectedCategory = items[position]
                            }
                        }
                }
                database.child(DB_CATEGORIES).removeEventListener(this)
            }
        })

        // Handler Cover
        editProductInfoCover.setOnClickListener {
            if (!checkPermissionREAD_EXTERNAL_STORAGE(this@CreateProductActivity)) {
                Toast.makeText(
                    this@CreateProductActivity, "GET_ACCOUNTS Denied",
                    Toast.LENGTH_SHORT
                ).show()
            } else pickFromGallery()
        }

        // Save data
        editProductInfoSave.setOnClickListener {
            doSaveProduct()
        }
    }

    // Request To Access Gallery
    private fun pickFromGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        val mimeTypes = arrayOf("image/jpeg", "image/png")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery()
                } else {
                    Toast.makeText(
                        this@CreateProductActivity, "GET_ACCOUNTS Denied",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    // Grant Access To Camera And Gallery
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                val selectedImage: Uri? = data!!.data
                val filePathColumn =
                    arrayOf(MediaStore.Images.Media.DATA)
                val cursor: Cursor? =
                    contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
                if (cursor != null) {
                    cursor.moveToFirst()
                    if (Build.VERSION.SDK_INT >= 29) {
                        val selectImage: Uri? = data.data
                        var inputStream: InputStream? = null
                        try {
                            if (BuildConfig.DEBUG && selectImage == null) {
                                error("Assertion failed")
                            }
                            inputStream = selectImage?.let { contentResolver.openInputStream(it) }
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        }

                        val bitmap = BitmapFactory.decodeStream(inputStream)
                        previewToCover(bitmap)
                    } else {
                        val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                        val imgDecodableString: String = cursor.getString(columnIndex)
                        cursor.close()
                        val bitmap: Bitmap = BitmapFactory.decodeFile(imgDecodableString)
                        previewToCover(bitmap)
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= 29) {
                        val selectImage: Uri? = data.data
                        var inputStream: InputStream? = null
                        try {
                            if (BuildConfig.DEBUG && selectImage == null) {
                                error("Assertion failed")
                            }
                            inputStream = selectImage?.let { contentResolver.openInputStream(it) }
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        }

                        val bitmap = BitmapFactory.decodeStream(inputStream)
                        previewToCover(bitmap)
                    } else {
                        LoadUtil.loadBitmap(selectedImage.toString())?.apply {
                            previewToCover(this)
                        }
                    }
                }
                Log.d(TAG, selectedImage.toString())
            }
        }
    }

    private fun checkPermissionREAD_EXTERNAL_STORAGE(context: Context): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    context, Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        context as Activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                ) {
                    showDialog(
                        "External storage", context,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                } else {
                    ActivityCompat
                        .requestPermissions(
                            context,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            GALLERY_REQUEST_CODE
                        )
                }
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    private fun showDialog(msg: String, activity: Activity, permission: String) {
        val alertBuilder = AlertDialog.Builder(activity)
        alertBuilder.setCancelable(true)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setPositiveButton(android.R.string.yes) { _, _ ->
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(permission),
                GALLERY_REQUEST_CODE
            )
        }
        alertBuilder.create().show()
    }

    private fun previewToCover(bitmap: Bitmap) {
        selectedCover = bitmap
        Glide.with(applicationContext).load(bitmap).into(previewProductCover)
    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.create_product)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    // Visibility Splashscreen or not
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            this.checkAlreadyUser(user)
        } else {
            progressDialog.dismiss()
            startActivity(Intent(this@CreateProductActivity, MainActivity::class.java))
            finish()
        }
    }

    // Check Already User Profile
    private fun checkAlreadyUser(user: FirebaseUser) {
        database.child(DB_USERS).child(user.uid).addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (!p0.exists()) {
                    progressDialog.dismiss()
                    startActivity(Intent(this@CreateProductActivity, MainActivity::class.java))
                    finish()
                } else progressDialog.dismiss()
                database.child(DB_USERS).child(user.uid).removeEventListener(this)
            }
        })
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        Toast.makeText(applicationContext, "Operation cancelled", Toast.LENGTH_SHORT).show()
        startActivity(Intent(this, HomeActivity::class.java))
    }

    private fun doSaveProduct(): Boolean {
        when {
            TextUtils.isEmpty(editProductInfoName.editText?.text) -> {
                editProductInfoName.error = "Silakan isi nama produk"
                return false
            }
            TextUtils.isEmpty(editProductInfoPrice.editText?.text) -> {
                editProductInfoPrice.error = "Silakan isi harga produk"
                return false
            }
            TextUtils.isEmpty(editProductInfoDescription.editText?.text) -> {
                editProductInfoDescription.error = "Silakan isi deskripsi produk"
                return false
            }
            selectedCategory == null -> {
                Snackbar.make(
                    editProductInfoCategory,
                    "Silakan isi kategori produk",
                    Snackbar.LENGTH_SHORT
                )
                    .show()
                return false
            }
            selectedCover == null -> {
                Snackbar.make(
                    editProductInfoCover,
                    "Silakan isi cover produk",
                    Snackbar.LENGTH_SHORT
                )
                    .show()
                return false
            }
            else -> {
                DialogUtil.showDialog(
                    this@CreateProductActivity,
                    "Buat Produk",
                    "Buat produk baru ${editProductInfoName.editText?.text.toString()} ?"
                ) { uploadToStorage(selectedCover) }

                return true
            }
        }
    }

    // Upload To Storage
    private fun uploadToStorage(bitmap: Bitmap?) {
        if (bitmap != null) {
            val key = database.child(DB_PRODUCTS).push().key!!
            // Defining the child of storageReference
            val ref = storage.reference
                .child("products/product_$key")

            val progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Mohon tunggu ...")
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog.setCancelable(false)
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.show()

            // or failure of image
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data: ByteArray = baos.toByteArray()
            ref.putBytes(data)
                .addOnSuccessListener {
                    // Image uploaded successfully
                    progressDialog.dismiss()
                    ref.downloadUrl.addOnSuccessListener {
                        if (it != null) {
                            doUpdate(it.toString(), key)
                        }
                    }
                }
                .addOnFailureListener { p0 ->
                    // Error, Image not uploaded
                    progressDialog.dismiss()
                    Toast
                        .makeText(
                            applicationContext,
                            "Failed " + p0.message,
                            Toast.LENGTH_SHORT
                        )
                        .show()
                }
                .addOnProgressListener {
                    val progress = (100.0 * it.bytesTransferred
                            / it.totalByteCount)
                    progressDialog.setMessage(String.format("Uploaded %.2f", progress) + "%")
                }
        }
    }

    private fun doUpdate(cover: String, key: String) {
        val data = Product(
            key,
            editProductInfoName.editText?.text.toString(),
            editProductInfoPrice.editText?.text.toString().toLong(),
            0,
            when {
                radioNotReady.isChecked -> 0
                radioPreOrder.isChecked -> 1
                else -> 2
            },
            cover,
            editProductInfoDescription.editText?.text.toString(),
            editProductInfoName.editText?.text.toString().toLowerCase(Locale.ROOT),
            selectedCategory!!
        )

        database.child(DB_PRODUCTS).child(key).setValue(data).addOnCompleteListener {
            if (it.isSuccessful) {
                Toast.makeText(
                    applicationContext,
                    "Success to create product",
                    Toast.LENGTH_SHORT
                )
                    .show()

                // send notif to client
                val dataNotif = Gson().toJson(data)
                mqtt?.publish("Cheva/BC/Products/$key", MqttMessage(dataNotif.toString().toByteArray()))

                startActivity(Intent(this@CreateProductActivity, HomeActivity::class.java))
                finish()
            }
        }

    }

    override fun onStart() {
        super.onStart()
        // Run Authentication First
        updateUI(auth.currentUser)
    }

    // Local Scope Variable
    companion object {
        const val TAG = "CREATE PRODUCT"
    }
}

package com.aitekteam.developer.bakercorneradmin.activities.profile

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.utils.TERMS_AND_CONDITION_URL
import kotlinx.android.synthetic.main.activity_terms_and_condition.*
import kotlinx.android.synthetic.main.component_toolbar.*

class TermsAndConditionActivity : AppCompatActivity() {

    // Declare Variable
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_and_condition)

        // Setup Toolbar
        setupToolbar()

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)
        progressDialog.show()

        // Load Website
        webTermsAndCondition.apply {
            this.webViewClient = MyWebViewClient()
            this.settings.javaScriptEnabled = true
            this.loadUrl(TERMS_AND_CONDITION_URL)
        }
    }

    inner class MyWebViewClient : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            progressDialog.dismiss()
        }
    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.terms_and_condition)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@TermsAndConditionActivity, ProfileActivity::class.java))
        finish()
    }

    // Local Scope Variable
    companion object {
        const val TAG = "Terms And Condition"
    }
}

package com.aitekteam.developer.bakercorneradmin.models

import com.aitekteam.developer.bakercorneradmin.models.items.FeedbackItem
import java.io.Serializable

data class OrderProduct (
    var uid: String = "",
    var time_stamp: Long = System.currentTimeMillis(),
    var total_price: Long = 0,
    var total_qty: Int = 0,
    var is_accept: Int = 0,
    var order_code: String = "",
    var type: List<Int> = arrayListOf(),
    var user: Users = Users(),
    var products: List<Product> = arrayListOf(),
    var productRequests: List<ProductRequest> = arrayListOf(),
    var feedbacks: List<FeedbackItem> = arrayListOf()
): Serializable

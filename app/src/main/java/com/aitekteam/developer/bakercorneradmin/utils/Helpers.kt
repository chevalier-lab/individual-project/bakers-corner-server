package com.aitekteam.developer.bakercorneradmin.utils

import com.aitekteam.developer.bakercorneradmin.models.Product
import com.aitekteam.developer.bakercorneradmin.models.ProductRequest
import com.aitekteam.developer.bakercorneradmin.models.items.DetailRiwayatTransaksiItem
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

object Helpers {

    fun convertLongToDateString(systemTime: Long): String {
        return SimpleDateFormat("EEEE, dd MMMM yyyy", Locale("in", "ID"))
            .format(systemTime).toString()
    }

    fun rupiah(uang: Double): String {
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        return numberFormat.format(uang).toString()
    }

    fun ProductRequest.convertRequestToDetailRiwayat(): DetailRiwayatTransaksiItem {
        return DetailRiwayatTransaksiItem(
            requestUid = uid,
            name = name,
            price = price,
            qty = qty,
            type = 1,
            is_available = is_available,
            is_accept = is_accept,
            description = description,
            categoryId = productCategory.uid,
            productCategory = productCategory.category,
            icon = productCategory.icon
        )
    }

    fun Product.convertProductToDetailRiwayat(): DetailRiwayatTransaksiItem {
        return DetailRiwayatTransaksiItem(
            productUid = uid,
            name = name,
            price = price,
            qty = qty,
            is_available = is_available,
            photos = photos,
            description = description,
            categoryId = productCategory.uid,
            productCategory = productCategory.category,
            icon = productCategory.icon
        )
    }
}
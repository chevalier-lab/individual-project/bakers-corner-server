package com.aitekteam.developer.bakercorneradmin.activities.profile

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_ORDERS
import com.aitekteam.developer.bakercorneradmin.utils.Helpers
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_riwayat_transaksi.*
import kotlinx.android.synthetic.main.component_riwayatlist.view.*
import kotlinx.android.synthetic.main.component_riwayatlist.view.tv_harga
import kotlinx.android.synthetic.main.component_riwayatlist.view.tv_order_code
import kotlinx.android.synthetic.main.component_riwayatlist.view.tv_qty
import kotlinx.android.synthetic.main.component_riwayatlist.view.tv_status
import kotlinx.android.synthetic.main.component_riwayatlist.view.tv_tanggal
import kotlinx.android.synthetic.main.component_riwayatlist.view.tv_type
import kotlinx.android.synthetic.main.component_toolbar.*

class RiwayatTransaksiActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    private lateinit var adapter: AdapterUtil<OrderProduct>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_riwayat_transaksi)

        // Setup Toolbar
        setupToolbar()

        // init database
        database = FirebaseDatabase.getInstance().getReference(DB_ORDERS)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this@RiwayatTransaksiActivity)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        progressDialog.show()
    }

    private fun loadRiwayatTransaksi() {
        // get data
        database.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) { }

            override fun onDataChange(data: DataSnapshot) {
                val listOrder = mutableListOf<OrderProduct>()
                if (data.exists()) {
                    for (orderSnapshot in data.children) {
                        val order = orderSnapshot.getValue(OrderProduct::class.java)
                        order?.let {
                            if (it.is_accept==3) listOrder.add(it)
                        }
                    }
                }

                // init adapter
                adapter = AdapterUtil(
                    R.layout.component_riwayatlist,
                    listOrder.filter { o -> o.is_accept == 3 }.sortedByDescending { o -> o.time_stamp },
                    { itemView, item ->

                        itemView.tv_status.text = StringBuilder().append("Sukses")
                        itemView.tv_status.setTextColor(
                            resources.getColor(
                                R.color.colorGreen
                            )
                        )

                        //set nama pembeli
                        itemView.tv_pembeli.text= StringBuilder().append(item.user.name)

                        itemView.tv_status.text = StringBuilder().append("Sukses")
                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorGreen))

                        itemView.tv_order_code.text =
                            StringBuilder().append("Order #").append(item.order_code)
                        itemView.tv_qty.text =
                            StringBuilder().append(item.total_qty)
                                .append(" pcs")
                        itemView.tv_harga.text =
                            Helpers.rupiah(
                                item.total_price.toDouble()
                            )
                        itemView.tv_tanggal.text = Helpers.convertLongToDateString(item.time_stamp)

                        // tipe order
                        if (item.type.size == 2) {
                            itemView.tv_type.text = java.lang.StringBuilder().append("Tipe : Product, Request")
                        } else if (item.type[0] == 0) {
                            itemView.tv_type.text = java.lang.StringBuilder().append("Tipe : Product")
                        } else {
                            itemView.tv_type.text = java.lang.StringBuilder().append("Tipe : Request")
                        }
                    },
                    { _, item ->
                        startActivity(
                            Intent(this@RiwayatTransaksiActivity, DetailRiwayatTransaksiActivity::class.java)
                            .putExtra("dataOrder",item)
                        )
                    }
                )

                // init rv
                rv_riwayattransaksi.apply {
                    this.layoutManager = LinearLayoutManager(context)
                    this.adapter = this@RiwayatTransaksiActivity.adapter
                }

                progressDialog.dismiss()
                database.removeEventListener(this)
            }
        })
    }

    override fun onResume() {
        loadRiwayatTransaksi()
        super.onResume()
    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.riwayat_transaction)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }
    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@RiwayatTransaksiActivity, ProfileActivity::class.java))
        finish()
    }
}

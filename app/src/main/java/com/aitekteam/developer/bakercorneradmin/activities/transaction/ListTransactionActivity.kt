package com.aitekteam.developer.bakercorneradmin.activities.transaction

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_ORDERS
import com.aitekteam.developer.bakercorneradmin.utils.Helpers
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.profile.DetailRiwayatTransaksiActivity
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.aitekteam.developer.bakercorneradmin.models.Users
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_list_transaction.*
import kotlinx.android.synthetic.main.fragment_transaction.rv_orders
import kotlinx.android.synthetic.main.item_order.view.*

@Suppress("SpellCheckingInspection")
class ListTransactionActivity : AppCompatActivity() {

    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    private lateinit var adapter: AdapterUtil<OrderProduct>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_transaction)

        database = FirebaseDatabase.getInstance().getReference(DB_ORDERS)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this@ListTransactionActivity)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        val dataUser = intent.getSerializableExtra("user") as Users

        progressDialog.show()

        // get data
        database.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) { }

            override fun onDataChange(data: DataSnapshot) {
                val listOrder = mutableListOf<OrderProduct>()
                if (data.exists()) {
                    setupToolbar(dataUser)
                    for (orderSnapshot in data.children) {
                        val order = orderSnapshot.getValue(OrderProduct::class.java)
                        if (order?.user?.uid == dataUser.uid) listOrder.add(order)
                    }
                }

                if (listOrder.isEmpty()) dataKosong.visibility = View.VISIBLE

                // init adapter
                adapter = AdapterUtil(R.layout.item_order,
                    listOrder.filter { o -> o.is_accept == 3 }, { itemView, item ->

                        itemView.tv_status.text = StringBuilder().append("Sukses")
                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorGreen))

                        itemView.tv_order_code.text =
                            StringBuilder().append("Order #").append(item.order_code)
                        itemView.tv_qty.text =
                            StringBuilder().append("Qty : ").append(item.total_qty)
                                .append(" pcs")
                        itemView.tv_harga.text = Helpers.rupiah(item.total_price.toDouble())
                        itemView.tv_tanggal.text =
                            Helpers.convertLongToDateString(item.time_stamp)

                        // tipe order
                        when {
                            item.type.size == 2 -> itemView.tv_type.text = java.lang.StringBuilder().append("Tipe : Product, Request")
                            item.type[0] == 0 -> itemView.tv_type.text = java.lang.StringBuilder().append("Tipe : Product")
                            else -> itemView.tv_type.text = java.lang.StringBuilder().append("Tipe : Request")
                        }

                        // status
                        itemView.tv_status.text = StringBuilder().append("Sukses")
                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorGreen))

                    }, { _, item ->
                        startActivity(
                            Intent(this@ListTransactionActivity, DetailRiwayatTransaksiActivity::class.java)
                            .putExtra("dataOrder",item)
                        )
                    }
                )

                // init rv
                rv_orders.apply {
                    this.layoutManager = LinearLayoutManager(this@ListTransactionActivity)
                    this.adapter = this@ListTransactionActivity.adapter
                }

                progressDialog.dismiss()
                database.removeEventListener(this)
            }
        })

    }
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar(users: Users) {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = StringBuilder().append("Transaksi ").append(users.name)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onBackPressed() {
        finish()
    }
}

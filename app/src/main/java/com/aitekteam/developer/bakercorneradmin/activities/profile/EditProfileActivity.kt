package com.aitekteam.developer.bakercorneradmin.activities.profile

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.aitekteam.developer.bakercorneradmin.BuildConfig
import com.aitekteam.developer.bakercorneradmin.utils.DB_USERS
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.MainActivity
import com.aitekteam.developer.bakercorneradmin.models.Users
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.component_toolbar.*
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.InputStream

class EditProfileActivity : AppCompatActivity() {

    // Declare Variable
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    //    private lateinit var cameraFilePath: String
    private lateinit var storage: FirebaseStorage
    private val SELECT_PHOTO = 100
    private lateinit var bitmap: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        // Setup Toolbar
        setupToolbar()

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Authentication
        auth = FirebaseAuth.getInstance()

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        // Setup Storage
        storage = FirebaseStorage.getInstance()

        //bt update
        btn_update.setOnClickListener {
            doUpdateProfile()
        }

        btn_choose_img.setOnClickListener {
            openGallery()

//            DialogUtil.dialogEditChooseImageOrPhotos(this@EditProfileActivity, object :
//                OnChooseImageOrPhoto {
//                override fun onItemClick(position: Int) {
//                    Log.d(TAG, "" + position)
//                    if (position == 0) openGallery()
////                    else captureFromCamera()
//                }
//            }).create().show()
        }
    }

    // Photo Upload
    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK).apply {
            this.type = "image/*"
        }
        startActivityForResult(intent, SELECT_PHOTO)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SELECT_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                val selectImage: Uri? = data?.data
                var inputStream: InputStream? = null

                try {
                    if (BuildConfig.DEBUG && selectImage == null) error("Assertion failed")
                    inputStream = selectImage?.let { contentResolver.openInputStream(it) }

                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }

                bitmap = BitmapFactory.decodeStream(inputStream)
                img_profile.setImageBitmap(bitmap)
            }
        }
    }

    // upload to storage
    private fun uploadToStorage(
        userId: String,
        bitmap: Bitmap?,
        progressDialog: ProgressDialog,
        exceptionListener: (Exception) -> Unit
    ) {
        if (bitmap != null) {
            // Defining the child of storageReference
            val ref = storage.reference
                .child("faces/photo_$userId")
            progressDialog.apply {
                this.setTitle("Mohon tunggu ...")
                this.setCancelable(false)
                this.setCanceledOnTouchOutside(false)
            }.show()

            // or failure of image
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data: ByteArray = baos.toByteArray()
            ref.putBytes(data)
                .addOnSuccessListener {
                    // Image uploaded successfully
                    progressDialog.dismiss()
                    ref.downloadUrl.addOnSuccessListener {
                        if (it != null) {
                            doUpdateFace(it.toString())
                        }
                    }
                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    exceptionListener(it)
                }
                .addOnProgressListener {
                    val progress = (100.0 * it.bytesTransferred / it.totalByteCount)
                    progressDialog.setMessage(String.format("Uploaded %.2f", progress) + "%")
                }
        }
    }

    // Request To Access Camera
//    private fun captureFromCamera() {
//        try {
//            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//            intent.putExtra(
//                MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(
//                    this,
//                    "com.aitekteam.developer.bakercorneradmin.provider", createImageFile()!!
//                )
//            )
//            startActivityForResult(intent, CAMERA_REQUEST_CODE)
//        } catch (ex: IOException) {
//            ex.printStackTrace()
//        }
//    }

    // Create Image File
//    private fun createImageFile(): File? {
//        // Create an image file name
//        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
//        val imageFileName = "JPEG_" + timeStamp + "_"
//
//        val storageDir = File(
//            Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_DCIM
//            ), "Camera"
//        )
//        val image = File.createTempFile(
//            imageFileName,  /* prefix */
//            ".jpg",         /* suffix */
//            storageDir      /* directory */
//        )
//
//        cameraFilePath = "file://" + image.absolutePath
//        return image
//    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = getString(R.string.edit_profile)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    // Check Already User Profile
    private fun checkAlreadyUser(user: FirebaseUser) {
        database.child(DB_USERS).child(user.uid).addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    Log.d(TAG, "" + p0.childrenCount)
                    p0.getValue(Users::class.java)?.apply { setupProfile(this) }
                } else {
                    progressDialog.dismiss()
                    startActivity(Intent(this@EditProfileActivity, MainActivity::class.java))
                    finish()
                }
                database.child(DB_USERS).child(user.uid).removeEventListener(this)
            }
        })
    }

    // Plot Profile
    private fun setupProfile(user: Users) {
        // Setup User Info Name
        et_nama.setText(user.name)
        // Setup User Info Email
        et_email.setText(user.email)
        // Setup User Info Phone Number
        et_phone.setText(user.phone_number)
        // Setup User Info Address
        et_address.setText(user.address)

        // image
        if (!this::bitmap.isInitialized) {
            Glide.with(this)
                .load(user.face)
                .into(img_profile)
        }

        progressbar.visibility = View.GONE
        progressDialog.dismiss()
    }

    // Visibility Splashscreen or not
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            this.checkAlreadyUser(user)
        } else {
            progressDialog.dismiss()
            startActivity(Intent(this@EditProfileActivity, MainActivity::class.java))
            finish()
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this@EditProfileActivity, ProfileActivity::class.java))
        finish()
    }

    // Do Update Face
    private fun doUpdateFace(url: String) {
        database.child("users").child(auth.uid!!).child("face").setValue(url)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Toast.makeText(
                        applicationContext,
                        "Berhasil mengubah profile",
                        Toast.LENGTH_SHORT
                    ).show()
                    startActivity(Intent(this@EditProfileActivity, ProfileActivity::class.java))
                }
            }
    }

    private fun doUpdateProfile() {
        val userReff = database.child(DB_USERS).child(auth.uid!!)
        userReff.child("name").setValue(et_nama.text.toString())
        userReff.child("phone_number").setValue(et_phone.text.toString())
        userReff.child("phone_number").setValue(et_phone.text.toString())
        userReff.child("address").setValue(et_address.text.toString())

        if (this::bitmap.isInitialized) {
            uploadToStorage(auth.uid!!, bitmap, ProgressDialog(this)) {
                Toast.makeText(
                    this,
                    "Failed : " + it.message, Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            updateUI(auth.currentUser)
            Toast.makeText(applicationContext, "Berhasil mengubah profile", Toast.LENGTH_SHORT)
                .show()
            startActivity(Intent(this@EditProfileActivity, ProfileActivity::class.java))
        }

    }

    override fun onStart() {
        super.onStart()
        // Run Authentication First
        updateUI(auth.currentUser)
    }

    // Local Scope Variable
    companion object {
        const val TAG = "EDIT PROFILE"

        interface OnChooseImageOrPhoto {
            fun onItemClick(position: Int)
        }
    }
}

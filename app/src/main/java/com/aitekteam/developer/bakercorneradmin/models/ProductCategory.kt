package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class ProductCategory (
    var uid: String = "",
    var category: String = "",
    var icon: String = ""
): Serializable
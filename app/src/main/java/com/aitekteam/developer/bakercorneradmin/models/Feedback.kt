package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class Feedback (
    var feedbackOrders: List<FeedbackOrders> = arrayListOf(),
    var feedbackProducts: List<FeedbackProducts> = arrayListOf()
): Serializable
package com.aitekteam.developer.bakercorneradmin.models.items

import java.io.Serializable

data class ProfileMenuItem (
    var label: String
): Serializable
package com.aitekteam.developer.bakercorneradmin.utils

const val RC_SIGN_IN = 9001
const val GALLERY_REQUEST_CODE = 9002
const val CAMERA_REQUEST_CODE = 9003
const val DB_USERS = "users"
const val DB_ICONS = "icons"
const val DB_ORDERS = "orders"
const val DB_CATEGORIES = "categories"
const val DB_PRODUCTS = "products"
const val TERMS_AND_CONDITION_URL = "http://213.190.4.40/bakers-corner/"
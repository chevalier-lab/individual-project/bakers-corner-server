package com.aitekteam.developer.bakercorneradmin.activities.profile

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.main.MainActivity
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.aitekteam.developer.bakercorneradmin.models.Users
import com.aitekteam.developer.bakercorneradmin.models.items.ProfileMenuItem
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_ORDERS
import com.aitekteam.developer.bakercorneradmin.utils.DB_USERS
import com.aitekteam.developer.bakercorneradmin.utils.DialogUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_profile.*

@Suppress("SpellCheckingInspection")
class ProfileActivity : AppCompatActivity() {

    // Declare Variable
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var storage: FirebaseStorage
    private lateinit var progressDialog: ProgressDialog
    private lateinit var cameraFilePath: String
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private var jumlahTransaksi = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setContentView(R.layout.activity_profile)

        // Setup Toolbar
        setupToolbar()

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Authentication
        auth = FirebaseAuth.getInstance()

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        // Setup Storage
        storage = FirebaseStorage.getInstance()

        progressDialog.show()

        // Setup Profile Menu
        listProfileMenu.layoutManager = LinearLayoutManager(this@ProfileActivity)
        listProfileMenu.adapter = AdapterUtil(R.layout.item_menu_profile,
            listOf(
                ProfileMenuItem("Riwayat Transaksi"),
                ProfileMenuItem("Terms And Condition"),
                ProfileMenuItem("Logout")
            ), { itemView, item ->
                val label = itemView.findViewById<TextView>(R.id.item_label)
                label.text = item.label
            }, { position, _ ->
                when (position) {
                    0 -> {
                        startActivity(Intent(this@ProfileActivity, RiwayatTransaksiActivity::class.java))
                        finish()
                    }
                    1 -> {
                        startActivity(Intent(this@ProfileActivity, TermsAndConditionActivity::class.java))
                        finish()
                    }
                    else -> {
                        DialogUtil.showDialog(this, getString(R.string.warning),
                            getString(R.string.logout_disclaimer)) {
                            logout(this, getString(R.string.default_web_client_id))
                            startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
                            finish()
                        }
                    }
                }
            })

        // Handler Image Round On Click
//        userInfoFace.setOnClickListener {
//            DialogUtil.dialogChooseImageOrPhotos(this@ProfileActivity, object: OnChooseImageOrPhoto {
//                override fun onItemClick(position: Int) {
//                    Log.d(TAG, "" + position)
//                    if (position == 0) pickFromGallery()
//                    else captureFromCamera()
//                }
//            }).create().show()
//        }

        // Handler Edit Profile
        userInfoEdit.setOnClickListener {
            startActivity(Intent(this@ProfileActivity, EditProfileActivity::class.java))
            finish()
        }
    }

    private fun logout(context: Context, webClientId: String) {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(webClientId)
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(context, gso)
        FirebaseAuth.getInstance().signOut()
        mGoogleSignInClient.signOut()
    }
 
    // Setup Toolbar
    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.profile)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    // Request To Access Camera
//    private fun captureFromCamera() {
//       try {
//           val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//           intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this,
//               "com.aitekteam.developer.bakercorneradmin.provider", createImageFile()!!))
//           startActivityForResult(intent, CAMERA_REQUEST_CODE)
//       } catch (ex: IOException) {
//           ex.printStackTrace()
//       }
//   }

    // Create Image File
//    private fun createImageFile(): File? {
//        // Create an image file name
//        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date());
//        val imageFileName = "JPEG_" + timeStamp + "_"
//
//        val storageDir = File(
//            Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_DCIM), "Camera")
//        val image = File.createTempFile(
//                imageFileName,  /* prefix */
//                ".jpg",         /* suffix */
//                storageDir      /* directory */
//        )
//
//        cameraFilePath = "file://" + image.absolutePath
//        return image;
//    }

    // Request To Access Gallery
//    private fun pickFromGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//        intent.type = "image/*"
//        val mimeTypes = arrayOf("image/jpeg", "image/png")
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
//            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
//        startActivityForResult(intent, GALLERY_REQUEST_CODE)
//    }

    // Grant Access To Camera And Gallery
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        if (resultCode == Activity.RESULT_OK) when (requestCode) {
//            GALLERY_REQUEST_CODE -> {
//                val selectedImage: Uri? = data!!.data
//                val filePathColumn =
//                    arrayOf(MediaStore.Images.Media.DATA)
//                val cursor: Cursor? =
//                    contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
//                if (cursor != null) {
//                    cursor.moveToFirst()
//                    if (Build.VERSION.SDK_INT >= 29) {
//                        val selectImage: Uri? = data.data
//                        var inputStream: InputStream? = null
//                        try {
//                            if (BuildConfig.DEBUG && selectImage == null) {
//                                error("Assertion failed")
//                            }
//                            inputStream = selectImage?.let { contentResolver.openInputStream(it) }
//                        } catch (e: FileNotFoundException) {
//                            e.printStackTrace()
//                        }
//
//                        val bitmap = BitmapFactory.decodeStream(inputStream)
//                        uploadToStorage(bitmap)
//                    }
//                    else {
//                        val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
//                        val imgDecodableString: String = cursor.getString(columnIndex)
//                        cursor.close()
//                        val bitmap: Bitmap = BitmapFactory.decodeFile(imgDecodableString)
//                        uploadToStorage(bitmap)
//                    }
//                }
//                else {
//                    if (Build.VERSION.SDK_INT >= 29) {
//                        val selectImage: Uri? = data.data
//                        var inputStream: InputStream? = null
//                        try {
//                            if (BuildConfig.DEBUG && selectImage == null) {
//                                error("Assertion failed")
//                            }
//                            inputStream = selectImage?.let { contentResolver.openInputStream(it) }
//                        } catch (e: FileNotFoundException) {
//                            e.printStackTrace()
//                        }
//
//                        val bitmap = BitmapFactory.decodeStream(inputStream)
//                        uploadToStorage(bitmap)
//                    }
//                    else {
//                        LoadUtil.loadBitmap(selectedImage.toString())?.apply {
//                            uploadToStorage(this)
//                        }
//                    }
//                }
//                Log.d(TAG, selectedImage.toString())
//            }
//            CAMERA_REQUEST_CODE -> {
//                if (Build.VERSION.SDK_INT >= 29) {
//                    val selectImage: Uri? = data?.data
//                    var inputStream: InputStream? = null
//                    try {
//                        if (BuildConfig.DEBUG && selectImage == null) {
//                            error("Assertion failed")
//                        }
//                        inputStream = selectImage?.let { contentResolver.openInputStream(it) }
//                    } catch (e: FileNotFoundException) {
//                        e.printStackTrace()
//                    }
//
//                    val bitmap = BitmapFactory.decodeStream(inputStream)
//                    uploadToStorage(bitmap)
//                }
//                else {
//                    LoadUtil.loadBitmap(Uri.parse(cameraFilePath).toString())?.apply {
//                        uploadToStorage(this)
//                    }
//                }
//            }
//        }
//    }

    // Upload To Storage
//    private fun uploadToStorage(bitmap: Bitmap?) {
//        if (bitmap != null) {
//            // Defining the child of storageReference
//            val ref = storage.reference
//                .child(
//                    "faces/"
//                            + UUID.randomUUID().toString())
//
//            val progressDialog = ProgressDialog(this)
//            progressDialog.setTitle("Mohon tunggu ...")
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
//            progressDialog.setCancelable(false)
//            progressDialog.setCanceledOnTouchOutside(false)
//            progressDialog.show()
//
//            // or failure of image
//            val baos = ByteArrayOutputStream()
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
//            val data: ByteArray = baos.toByteArray()
//            ref.putBytes(data)
//                .addOnSuccessListener {
//                    // Image uploaded successfully
//                    progressDialog.dismiss()
//                    ref.downloadUrl.addOnSuccessListener {
//                        if (it != null) {
//                            doUpdateFace(it.toString())
//                        }
//                    }
//                }
//                .addOnFailureListener { p0 ->
//                    // Error, Image not uploaded
//                    progressDialog.dismiss()
//                    Toast
//                        .makeText(applicationContext,
//                            "Failed " + p0.message,
//                            Toast.LENGTH_SHORT)
//                        .show()
//                }
//                .addOnProgressListener {
//                    val progress = (100.0 * it.bytesTransferred
//                            / it.totalByteCount)
//                    progressDialog.setMessage(String.format("Uploaded %.2f", progress) + "%")
//                }
//        }
//    }

    // Do Update Face
//    private fun doUpdateFace(url: String) {
//        database.child("users").child(auth.uid!!).child("face").setValue(url).addOnCompleteListener {
//            if (it.isSuccessful) {
//                Toast.makeText(
//                    applicationContext,
//                    "Success Update Photo Profile",
//                    Toast.LENGTH_SHORT
//                ).show()
//                updateUI(auth.currentUser)
//            }
//        }
//    }

    // Visibility Splashscreen or not
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            this.checkAlreadyUser(user)
        } else {
            progressDialog.dismiss()
            startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
            finish()
        }
    }

    // Check Already User Profile
    private fun checkAlreadyUser(user: FirebaseUser) {
        database.child(DB_USERS).child(user.uid).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    Log.d(TAG, "" + p0.childrenCount)
                    p0.getValue(Users::class.java)?.apply {setupProfile(this)}
                }
                else {
                    progressDialog.dismiss()
                    startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
                    finish()
                }
                database.child(DB_USERS).child(user.uid).removeEventListener(this)
            }
        })
    }

    // Plot Profile
    private fun setupProfile(user: Users) {
        // Setup User Info Face
        val requestOptions = RequestOptions()
        Glide.with(applicationContext).load(user.face)
            .apply(requestOptions.circleCrop())
            .into(userInfoFace)

        // Setup User Info Name
        userInfoName.text = user.name

        // Setup jumlah transaksi
        database.child(DB_ORDERS).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) { }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    for(transaksi in p0.children) {
                        val data = transaksi.getValue(OrderProduct::class.java)
                        if (data?.is_accept == 3) {
                            jumlahTransaksi += 1
                        }
                    }

                    userInfoTrx?.text = java.lang.StringBuilder().append(jumlahTransaksi).append(" transaksi")
                }
                database.child(DB_ORDERS).removeEventListener(this)
            }

        })

        userInfoTrx.text = StringBuilder().append(user.userUtility.transaction).append(" Transaksi")
        progressDialog.dismiss()
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@ProfileActivity, HomeActivity::class.java))
        finish()
    }

    override fun onStart() {
        super.onStart()
        // Run Authentication First
        updateUI(auth.currentUser)
    }

    // Local Scope Variable
    companion object {
        const val TAG = "PROFILE"
        interface OnChooseImageOrPhoto {
            fun onItemClick(position: Int)
        }
    }
}

package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class FeedbackOrders(
    var uid: String = "",
    var orderProduct: OrderProduct = OrderProduct(),
    var description: String = ""
) : Serializable {
    override fun equals(other: Any?): Boolean =
        if (other is FeedbackOrders) other.uid == uid else false

    override fun hashCode(): Int {
        var result = uid.hashCode()
        result = 31 * result + orderProduct.hashCode()
        result = 31 * result + description.hashCode()
        return result
    }
}

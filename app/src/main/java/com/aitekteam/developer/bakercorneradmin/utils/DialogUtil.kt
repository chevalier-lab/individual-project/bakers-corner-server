package com.aitekteam.developer.bakercorneradmin.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.product.CreateCategoryProductActivity
import com.aitekteam.developer.bakercorneradmin.activities.profile.EditProfileActivity
import com.aitekteam.developer.bakercorneradmin.activities.profile.ProfileActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder

object DialogUtil {
    fun dialogChooseImageOrPhotos(activity: Activity, handler: ProfileActivity.Companion.OnChooseImageOrPhoto):
            AlertDialog.Builder {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.label_change_photo_profile)
            .setItems(R.array.array_choose_image_or_photos) { dialog, which ->
                handler.onItemClick(which)
                dialog.dismiss()
            }
        return builder
    }

    fun dialogEditChooseImageOrPhotos(activity: Activity, handler: EditProfileActivity.Companion.OnChooseImageOrPhoto): AlertDialog.Builder {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.label_change_photo_profile)
            .setItems(R.array.array_choose_image_or_photos) { dialog, which ->
                handler.onItemClick(which)
                dialog.dismiss()
            }
        return builder
    }

    @SuppressLint("InflateParams")
    fun dialogChooseIcon(activity: Activity, handler: CreateCategoryProductActivity.Companion.OnChooseIcon): MaterialAlertDialogBuilder {
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.component_list, null)
        handler.onLoad(view)

        val builder = MaterialAlertDialogBuilder(activity)
        builder.apply {
            this.setTitle(R.string.button_choose_icon)
            this.setView(view)
            this.setPositiveButton(android.R.string.yes) { dialog, _ ->
                handler.onItemClick(dialog, 0)
                dialog.dismiss()
            }
            this.setNegativeButton(android.R.string.no) {dialog, _ ->
                dialog.dismiss()
            }
        }

        return builder
    }

    fun showDialogPossitiveOnly(context: Context, title: String, message: String) {
        MaterialAlertDialogBuilder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Oke") { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(false)
            .show()
    }

    fun showDialog(context: Context, title: String,
                   message: String, callback: () -> Unit) {
        MaterialAlertDialogBuilder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ya") { dialog, _ ->
                callback()
                dialog.dismiss()
            }
            .setNegativeButton("Batal") { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(false)
            .show()
    }


}
package com.aitekteam.developer.bakercorneradmin.models

import com.aitekteam.developer.bakercorneradmin.models.items.FeedbackItem
import java.io.Serializable

data class Product (
    var uid: String = "",
    var name: String = "",
    var price: Long = 0,
    var qty: Int = 0,
    var is_available: Int = 0,
    var photos: String = "",
    var description: String = "",
    var search: String = "",
    var productCategory: ProductCategory = ProductCategory(),
    var feedbacks: List<FeedbackItem> = arrayListOf()
): Serializable
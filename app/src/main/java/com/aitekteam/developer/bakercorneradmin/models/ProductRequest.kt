package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class ProductRequest (
    var uid: String = "",
    var name: String = "",
    var price: Long = 0,
    var qty: Int = 0,
    var is_available: Int = 0,
    var is_accept: Int = 0,
    var photos: String = "",
    var description: String = "",
    var productCategory: ProductCategory = ProductCategory()
): Serializable
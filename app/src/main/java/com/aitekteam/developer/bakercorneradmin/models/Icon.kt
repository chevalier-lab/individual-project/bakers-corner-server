package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class Icon (
    var uid: String = "",
    var label: String = "",
    var url: String = ""
): Serializable
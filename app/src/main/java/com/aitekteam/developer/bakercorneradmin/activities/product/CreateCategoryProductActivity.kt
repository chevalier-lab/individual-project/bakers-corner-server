package com.aitekteam.developer.bakercorneradmin.activities.product

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.main.MainActivity
import com.aitekteam.developer.bakercorneradmin.models.Icon
import com.aitekteam.developer.bakercorneradmin.models.ProductCategory
import com.aitekteam.developer.bakercorneradmin.utils.*
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_create_category_product.*
import kotlinx.android.synthetic.main.activity_create_product.*
import kotlinx.android.synthetic.main.component_toolbar.*

class CreateCategoryProductActivity : AppCompatActivity() {

    // Declare Variable
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    private var iconSelected: Icon? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_category_product)

        // Setup Toolbar
        setupToolbar()

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Authentication
        auth = FirebaseAuth.getInstance()

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        progressDialog.show()

        editCategoryInfoIcon.setOnClickListener {
            DialogUtil.dialogChooseIcon(this@CreateCategoryProductActivity, object : OnChooseIcon {
                override fun onItemClick(dialog: DialogInterface, position: Int) {
                    iconSelected?.apply {
                        Glide.with(applicationContext).load(this.url)
                            .into(previewCategoryIcon)
                    }
                    dialog.dismiss()
                }

                override fun onLoad(view: View) {
                    database.child(DB_ICONS).addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {}

                        override fun onDataChange(p0: DataSnapshot) {
                            if (p0.childrenCount > 0) {
                                val items: MutableList<Icon> = arrayListOf()
                                for (item: DataSnapshot in p0.children) {
                                    item.getValue(Icon::class.java)?.apply {
                                        items.add(this)
                                    }
                                }

                                val list = view.findViewById<RecyclerView>(R.id.list)
                                list.layoutManager =
                                    GridLayoutManager(this@CreateCategoryProductActivity, 3)
                                list.adapter = AdapterUtil(R.layout.item_icon, items,
                                    { itemView, item ->
                                        val icon = itemView.findViewById<ImageView>(R.id.item_icon)
                                        val label = itemView.findViewById<TextView>(R.id.item_label)
                                        Glide.with(applicationContext).load(item.url)
                                            .into(icon)
                                        label.text = item.label
                                    }, { _, item ->
                                        iconSelected = item
                                        Toast.makeText(
                                            this@CreateCategoryProductActivity,
                                            "Selected " + item.label, Toast.LENGTH_SHORT
                                        ).show()
                                    })
                            }
                            database.child("icons").removeEventListener(this)
                        }
                    })
                }
            }).create().show()
        }

        editCategoryInfoSave.setOnClickListener {
            doCreateCategoryProduct()
        }
    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.create_category_product)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    // Visibility Splashscreen or not
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            this.checkAlreadyUser(user)
        } else {
            progressDialog.dismiss()
            startActivity(Intent(this@CreateCategoryProductActivity, MainActivity::class.java))
            finish()
        }
    }

    // Check Already User Profile
    private fun checkAlreadyUser(user: FirebaseUser) {
        database.child(DB_USERS).child(user.uid).addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (!p0.exists()) {
                    progressDialog.dismiss()
                    startActivity(
                        Intent(
                            this@CreateCategoryProductActivity,
                            MainActivity::class.java
                        )
                    )
                    finish()
                } else progressDialog.dismiss()
                database.child(DB_USERS).child(user.uid).removeEventListener(this)
            }
        })
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@CreateCategoryProductActivity, HomeActivity::class.java))
        finish()
    }

    // Do Create Category Product
    private fun doCreateCategoryProduct() {
        when {
            !TextUtils.isEmpty(editCategoryInfoName.editText?.text) && iconSelected != null -> {
                val key = database.child(DB_CATEGORIES).push().key
                iconSelected?.apply {
                    this.label = editCategoryInfoName.editText?.text.toString()
                    this.uid = key!!
                    database.child(DB_CATEGORIES).child(key).setValue(
                        ProductCategory(
                            key,
                            this.label,
                            this.url
                        )
                    ).addOnCompleteListener {
                        if (it.isSuccessful)

                            DialogUtil.showDialog(
                                this@CreateCategoryProductActivity,
                                "Buat Kategori",
                                "Buat kategori baru ${editCategoryInfoName.editText?.text.toString()} ?"
                            ) {
                                Toast.makeText(
                                    applicationContext,
                                    "Success to create category product",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                    }
                }
            }

            TextUtils.isEmpty(editCategoryInfoName.editText?.text) -> {
                editCategoryInfoName.error = "Silakan isi nama produk"
            }

            previewProductCover == null -> {
                Snackbar.make(
                    editCategoryInfoIcon,
                    "Please fill category icon",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        // Run Authentication First
        updateUI(auth.currentUser)
    }

    // Local Scope Variable
    companion object {
        const val TAG = "CREATE CATEGORY PRODUCT"

        interface OnChooseIcon {
            fun onItemClick(dialog: DialogInterface, position: Int)
            fun onLoad(view: View)
        }
    }
}

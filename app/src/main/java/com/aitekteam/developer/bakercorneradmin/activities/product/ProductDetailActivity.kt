package com.aitekteam.developer.bakercorneradmin.activities.product

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.aitekteam.developer.bakercorneradmin.*
import com.aitekteam.developer.bakercorneradmin.models.Product
import com.aitekteam.developer.bakercorneradmin.utils.DB_PRODUCTS
import com.aitekteam.developer.bakercorneradmin.utils.DialogUtil
import com.bumptech.glide.Glide
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.component_toolbar.*
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.InputStream

class ProductDetailActivity : AppCompatActivity() {

    private val SELECT_PHOTO = 100
    private lateinit var bitmap: Bitmap
    private lateinit var itemProduct : Product
    private lateinit var storage: FirebaseStorage
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        //setup toolbar
        setupToolbar()

        itemProduct = intent.getSerializableExtra("itemProduct")!! as Product

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        // Setup Storage
        storage = FirebaseStorage.getInstance()

        //init UI
        initUI(itemProduct)

    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.transaction)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }

    //init ui
    private fun initUI(itemProduct: Product) {
        //set title
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = itemProduct.name
            this.setDisplayHomeAsUpEnabled(true)
            et_nama_produk.setText(itemProduct.name)
            et_harga.setText(itemProduct.price.toString())
            et_deksripsiproduk.setText(itemProduct.description)
            et_kategori.setText(itemProduct.productCategory.category)

            when (itemProduct.is_available) {
                0 -> rb_habis.isChecked = true
                1 -> rb_preorder.isChecked = true
                else -> rb_ready.isChecked = true
            }

            // set image of product
            Glide.with(this@ProductDetailActivity)
                .load(itemProduct.photos)
                .into(image_product)

            // ganti foto
            edit_image_product.setOnClickListener {
                openCamera()
            }

            //update data
            bt_save.setOnClickListener {
                DialogUtil.showDialog(this@ProductDetailActivity,"Update Data","Simpan Update Status "+itemProduct.name+" ?"){
                    inputCheck()
                }
            }

        }
    }

    private fun openCamera() {
        val intent = Intent(Intent.ACTION_PICK).apply {
            this.type = "image/*"
        }
        startActivityForResult(intent, SELECT_PHOTO)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SELECT_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                val selectImage: Uri? = data?.data
                var inputStream: InputStream? = null
                try {
                    if (BuildConfig.DEBUG && selectImage == null) {
                        error("Assertion failed")
                    }
                    inputStream = selectImage?.let { contentResolver.openInputStream(it) }
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }

                bitmap = BitmapFactory.decodeStream(inputStream)
                image_product.setImageBitmap(bitmap)
            }
        }
    }

    private fun inputCheck() {
        when {
            et_nama_produk.text.toString().isEmpty() -> et_nama_produk.error = "Nama produk wajib diisi!"
            et_harga.text.toString().isEmpty() -> et_harga.error = "Harga produk wajib diisi!"
            et_deksripsiproduk.text.toString().isEmpty() -> et_deksripsiproduk.error = "Deskripsi produk wajib diisi!"
            else -> updateData()
        }
    }

    //update is available
    private fun updateData(){
        val dataIsAvailable = database.child(DB_PRODUCTS).child(itemProduct.uid)

        val updateCategory :Int = when {
            rb_habis.isChecked -> 0
            rb_preorder.isChecked -> 1
            else -> 2
        }

        // cek upload image
        if (this::bitmap.isInitialized) {
            uploadToStorage(bitmap, itemProduct.uid)
        }

        dataIsAvailable.child("name").setValue(et_nama_produk.text.toString())
        dataIsAvailable.child("price").setValue(et_harga.text.toString().toLong())
        dataIsAvailable.child("description").setValue(et_deksripsiproduk.text.toString())
        dataIsAvailable.child("_available").setValue(updateCategory).addOnCompleteListener{
            if (it.isSuccessful){
                Toast.makeText(applicationContext, "Berhasil mengubah produk", Toast.LENGTH_SHORT).show()
                startActivity(Intent(this@ProductDetailActivity,ProductActivity::class.java))
            } else {
                Toast.makeText(applicationContext, "Gagal mengubah produk", Toast.LENGTH_SHORT).show()
            }
        }
    }

    // Upload To Storage
    private fun uploadToStorage(bitmap: Bitmap?, key: String) {
        if (bitmap != null) {
            // Defining the child of storageReference
            val ref = storage.reference
                .child("products/product_$key")

            val progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Mohon tunggu ...")
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog.setCancelable(false)
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.show()

            // or failure of image
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data: ByteArray = baos.toByteArray()
            ref.putBytes(data)
                .addOnSuccessListener {
                    // Image uploaded successfully
                    progressDialog.dismiss()
                    ref.downloadUrl.addOnSuccessListener { uri ->
                        if (uri != null) {
                            database.child(DB_PRODUCTS).child(itemProduct.uid).child("photos").setValue(uri.toString())
                        }
                    }
                }
                .addOnFailureListener { p0 ->
                    // Error, Image not uploaded
                    progressDialog.dismiss()
                    Toast
                        .makeText(
                            applicationContext,
                            "Failed " + p0.message,
                            Toast.LENGTH_SHORT
                        )
                        .show()
                }
                .addOnProgressListener {
                    val progress = (100.0 * it.bytesTransferred
                            / it.totalByteCount)
                    progressDialog.setMessage(String.format("Uploaded %.2f", progress) + "%")
                }
        }
    }

}

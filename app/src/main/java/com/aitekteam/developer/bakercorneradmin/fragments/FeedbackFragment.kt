package com.aitekteam.developer.bakercorneradmin.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_USERS
import com.aitekteam.developer.bakercorneradmin.utils.Helpers
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.profile.DetailRiwayatTransaksiActivity
import com.aitekteam.developer.bakercorneradmin.models.FeedbackOrders
import com.aitekteam.developer.bakercorneradmin.models.FeedbackProducts
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.aitekteam.developer.bakercorneradmin.models.Users
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.tabs.TabLayout
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_feedback.*
import kotlinx.android.synthetic.main.item_feedback.view.*

@Suppress("SpellCheckingInspection")
class FeedbackFragment : Fragment() {
    private lateinit var firebaseDatabase: DatabaseReference
    private lateinit var adapterOrder: AdapterUtil<FeedbackOrders>
    private lateinit var adapterProducts: AdapterUtil<FeedbackProducts>
    private lateinit var progressDialog: ProgressDialog

    // utils
    private lateinit var listOrder: MutableList<FeedbackOrders>
    private lateinit var listProducts: MutableList<FeedbackProducts>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_feedback, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup SupportActionBar title
        (activity as HomeActivity).supportActionBar?.apply {
            this.title = getString(R.string.feedback)
        }

        // init utils
        listOrder = mutableListOf()
        listProducts = mutableListOf()

        // FirebaseDatabase instance initialization
        firebaseDatabase = FirebaseDatabase.getInstance().reference

        // update image profile user terbaru (dimulai dari feedbackOrder)
        latestImageProfile.feedbackOrder()

        // Setup Progress Dialog
        progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Show "Feedback Orders" page when the feedback bottom nav is clicked
        progressDialog.show()
        setFeedbackOrder()

        feedback_tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                // Initiate feedback data based on the selected tabs
                when (feedback_tab.selectedTabPosition) {
                    0 -> {
                        // Show "Feedback Orders" data
                        progressDialog.show()
                        setFeedbackOrder()

                        // update image profile user terbaru
                        latestImageProfile.feedbackOrder()

                        // nanti disini clear listProduct
                        listProducts.clear()
                    }
                    1 -> {
                        // Show "Feedback Products" data
                        progressDialog.show()
                        setFeedbackProducts()

                        // update image profile user terbaru
                        latestImageProfile.feedbackProducts() // masih kosong functionnya, tinggal kamu isi

                        // clear listOrder
                        // karena supaya ga nambah tiap klik tab Feedback Products
                        listOrder.clear()
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}
            override fun onTabUnselected(tab: TabLayout.Tab?) {}
        })
    }

    private fun findUser(userUid: String, callback: (DataSnapshot) -> Unit) {
        firebaseDatabase
            .child(DB_USERS)
            .child(userUid)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {}

                override fun onDataChange(p0: DataSnapshot) {
                    callback(p0)
                }
            })
    }

    private val latestImageProfile = object {
        fun feedbackOrder() {
            firebaseDatabase
                .child("feedbacks")
                .child("feedbackOrders")
                .addChildEventListener(object : ChildEventListener {
                    override fun onCancelled(p0: DatabaseError) {}

                    override fun onChildMoved(p0: DataSnapshot, p1: String?) {}

                    override fun onChildRemoved(p0: DataSnapshot) {}

                    override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                        val feedback = p0.getValue(FeedbackOrders::class.java)

                        feedback?.let {
                            // find user
                            findUser(it.orderProduct.user.uid) { user ->
                                // update isi obj user dengan yg terbaru
                                it.orderProduct.user = user.getValue(Users::class.java)!!

                                // update listOrder
                                if (!listOrder.contains(it)) listOrder.add(it)
                                else {
                                    val index = listOrder.indexOf(it)
                                    listOrder[index] = it
                                }

                                Log.i("testing", "Nambah")

                                // update all data (realtime sort)
                                adapterOrder.data =
                                    listOrder.sortedBy { o -> o.orderProduct.order_code }
                            }
                        }
                    }

                    override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                        val feedback = p0.getValue(FeedbackOrders::class.java)

                        feedback?.let {
                            // find user
                            findUser(it.orderProduct.user.uid) { user ->
                                // update isi obj user dengan yg terbaru
                                it.orderProduct.user = user.getValue(Users::class.java)!!

                                // update listOrder
                                if (!listOrder.contains(it)) {
                                    listOrder.add(it)
                                } else {
                                    val index = listOrder.indexOf(it)
                                    listOrder[index] = it
                                }

                                Log.i("testing", "Nambah")

                                // update all data (realtime sort)
                                adapterOrder.data =
                                    listOrder.sortedBy { o -> o.orderProduct.order_code }
                            }
                        }
                    }
                })
        }

        fun feedbackProducts() {
            firebaseDatabase
                .child("feedbacks")
                .child("feedbackProducts")
                .addChildEventListener(object : ChildEventListener {
                    override fun onCancelled(databaseError: DatabaseError) {}

                    override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}

                    override fun onChildRemoved(dataSnapshot: DataSnapshot) {}

                    override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {
                        val feedback = dataSnapshot.getValue(FeedbackProducts::class.java)

                        // find user
                        feedback?.let {
                            findUser(it.user.uid) { user ->
                                // update isi obj user dengan yg terbaru
                                it.user = user.getValue(Users::class.java)!!

                                // update listOrder
                                if (!listProducts.contains(it)) listProducts.add(it)
                                else {
                                    val index = listProducts.indexOf(it)
                                    listProducts[index] = it
                                }

                                Log.i("feedbackProducts()", "Nambah")

                                // update all data (realtime sort)
                                adapterProducts.data = listProducts.sortedBy { o -> o.product.name }
                            }
                        }
                    }

                    override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                        val feedback = dataSnapshot.getValue(FeedbackProducts::class.java)

                        feedback?.let {
                            // find user
                            findUser(it.user.uid) { user ->
                                // update isi obj user dengan yg terbaru
                                it.user = user.getValue(Users::class.java)!!

                                // update listOrder
                                if (!listProducts.contains(it)) listProducts.add(it)
                                else {
                                    val index = listProducts.indexOf(it)
                                    listProducts[index] = it
                                }

                                Log.i("feedbackOrder()", "Nambah")

                                // update all data (realtime sort)
                                adapterProducts.data = listProducts.sortedBy { o -> o.product.name }
                            }
                        }
                    }
                })
        }
    }

    private fun setFeedbackOrder() {
        // listOrder dijadiin global variabel
//        val listOrder = mutableListOf<FeedbackOrders>()

        // Gather "Feedback Orders" data from Firebase
        firebaseDatabase.child("feedbacks").child("feedbackOrders")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // DataSnapshot existence check
                    Log.i(
                        "DataCheck",
                        "${dataSnapshot.children} - ${dataSnapshot.childrenCount} childrens"
                    )

                    if (dataSnapshot.exists()) {
                        for (feedbackOrder in dataSnapshot.children) {
                            // If dataSnapshot exists, add them to mutableList
                            val value = feedbackOrder.getValue(FeedbackOrders::class.java)
                            value?.let { listOrder.add(it) }

                            Log.i("Value", "$listOrder\n\n${value.toString()}")
                            progressDialog.dismiss()
                        }
                    } else {
                        // If dataSnapshot doesn't exist
                        Toast.makeText(
                            requireContext(),
                            "Sorry, no data in here :(",
                            Toast.LENGTH_SHORT
                        ).show()
                        progressDialog.dismiss()
                    }

                    // Adapter and Item data loader
                    adapterOrder = AdapterUtil(
                        R.layout.item_feedback,
                        listOrder.sortedBy { o -> o.orderProduct.order_code }, { itemView, item ->
                            try {
                                Glide.with(requireContext())
                                    .load(item.orderProduct.user.face)
                                    .apply(
                                        RequestOptions()
                                            .circleCrop()
                                    )
                                    .into(itemView.iv_cover)
                                Log.i("GlideCheck", "Done!")
                            } catch (e: Exception) {
                                Log.i(
                                    "GlideCheck",
                                    "Failed! ${e.message} - ${e.printStackTrace()}"
                                )
                            }

                            itemView.tv_feedback.text = StringBuilder()
                                .append("Feedback On Order #")
                                .append(item.orderProduct.order_code)
                            itemView.tv_name.text =
                                StringBuilder().append(item.orderProduct.user.name)
                            itemView.tv_description.text = StringBuilder()
                                .append(item.description)
                            itemView.tv_date.text = Helpers.convertLongToDateString(item.orderProduct.time_stamp)
                        },
                        { _, item ->
                            startActivity(
                                Intent(requireContext(), DetailRiwayatTransaksiActivity::class.java)
                                    .putExtra("dataOrder", item.orderProduct)
                            )
                        }
                    )

                    // RecyclerView initialization
                    feedback_list?.apply {
                        this.layoutManager = LinearLayoutManager(requireContext())
                        this.adapter = this@FeedbackFragment.adapterOrder
                    }
                    firebaseDatabase.removeEventListener(this)
                }
            })
    }

    private fun setFeedbackProducts() {
        // Setup mutableList of FeedbackProducts
//        val listProducts = mutableListOf<FeedbackProducts>()

        // Gather "Feedback Products" data from Firebase
        firebaseDatabase.child("feedbacks").child("feedbackProducts")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // DataSnapshot existence check
                    Log.i(
                        "DataCheck",
                        "${dataSnapshot.children} - ${dataSnapshot.childrenCount} childrens"
                    )

                    if (dataSnapshot.exists()) {
                        for (feedbackProducts in dataSnapshot.children) {
                            // If dataSnapshot exists, add them to mutableList
                            val value = feedbackProducts.getValue(FeedbackProducts::class.java)
                            value?.let { listProducts.add(it) }

                            Log.i("Value", "$listProducts\n\n${value.toString()}")
                            progressDialog.dismiss()
                        }
                    } else {
                        // If dataSnapshot doesn't exist
                        Toast.makeText(
                            activity,
                            "Sorry, no data in here :(",
                            Toast.LENGTH_SHORT
                        ).show()
                        progressDialog.dismiss()
                    }

                    // Adapter and Item data loader
                    adapterProducts = AdapterUtil(
                        R.layout.item_feedback,
                        listProducts.sortedBy { o -> o.product.name }, { itemView, item ->
                            try {
                                Glide.with(requireContext())
                                    .load(item.user.face)
                                    .apply(
                                        RequestOptions()
                                            .circleCrop()
                                    )
                                    .into(itemView.iv_cover)
                                Log.i("GlideCheck", "Done!")
                            } catch (e: Exception) {
                                Log.i(
                                    "GlideCheck",
                                    "Failed! ${e.message} - ${e.printStackTrace()}"
                                )
                            }

                            itemView.tv_feedback.text = StringBuilder()
                                .append("Feedback On Product ")
                                .append(item.product.name)
                            itemView.tv_name.text =
                                StringBuilder().append(item.user.name)
                            itemView.tv_description.text = StringBuilder()
                                .append(item.description)
                            itemView.tv_date.text = Helpers
                                .convertLongToDateString(OrderProduct().time_stamp)
                        },
                        { _, _ -> }
                    )

                    // RecyclerView initialization
                    feedback_list?.apply {
                        this.layoutManager = LinearLayoutManager(requireContext())
                        this.adapter = this@FeedbackFragment.adapterProducts
                    }
                    firebaseDatabase.removeEventListener(this)
                }
            })
    }
}

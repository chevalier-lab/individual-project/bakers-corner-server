package com.aitekteam.developer.bakercorneradmin.activities.product

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_CATEGORIES
import com.aitekteam.developer.bakercorneradmin.utils.DB_USERS
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.main.MainActivity
import com.aitekteam.developer.bakercorneradmin.models.ProductCategory
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_category_product.*
import kotlinx.android.synthetic.main.component_toolbar.*

class CategoryProductActivity : AppCompatActivity() {

    // Declare Variable
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_product)

        // Setup Toolbar
        setupToolbar()

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Authentication
        auth = FirebaseAuth.getInstance()

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        progressDialog.show()

        // Setup List Category Product
        listCategoryProduct.layoutManager = GridLayoutManager(this@CategoryProductActivity, 3)
        database.child(DB_CATEGORIES).addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.childrenCount > 0) {
                    val items: MutableList<ProductCategory> = arrayListOf()
                    for (item: DataSnapshot in p0.children) {
                        item.getValue(ProductCategory::class.java)?.apply {
                            items.add(this)
                        }
                    }

                    listCategoryProduct.adapter = AdapterUtil(R.layout.item_icon, items,
                        {itemView, item ->
                            val icon = itemView.findViewById<ImageView>(R.id.item_icon)
                            val label = itemView.findViewById<TextView>(R.id.item_label)
                            Glide.with(applicationContext).load(item.icon)
                                .into(icon)
                            label.text = item.category
                        },
                        {_, _ -> })
                }
                database.child(DB_CATEGORIES).removeEventListener(this)
            }
        })
    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.category_product)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    // Visibility Splashscreen or not
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            this.checkAlreadyUser(user)
        } else {
            progressDialog.dismiss()
            startActivity(Intent(this@CategoryProductActivity, MainActivity::class.java))
            finish()
        }
    }

    // Check Already User Profile
    private fun checkAlreadyUser(user: FirebaseUser) {
        database.child(DB_USERS).child(user.uid).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (!p0.exists()) {
                    progressDialog.dismiss()
                    startActivity(Intent(this@CategoryProductActivity, MainActivity::class.java))
                    finish()
                }
                else progressDialog.dismiss()
                database.child(DB_USERS).child(user.uid).removeEventListener(this)
            }
        })
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@CategoryProductActivity, HomeActivity::class.java))
        finish()
    }

    override fun onStart() {
        super.onStart()
        // Run Authentication First
        updateUI(auth.currentUser)
    }

    // Local Scope Variable
    companion object {
        const val TAG = "CATEGORY PRODUCT"
    }
}

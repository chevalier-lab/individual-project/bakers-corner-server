package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class FeedbackProducts(
    var uid: String = "",
    var product: Product = Product(),
    var user: Users = Users(),
    var description: String = ""
) : Serializable {
    override fun equals(other: Any?): Boolean =
        if (other is FeedbackProducts) other.uid == uid else false

    override fun hashCode(): Int {
        var result = uid.hashCode()
        result = 31 * result + product.hashCode()
        result = 31 * result + user.hashCode()
        result = 31 * result + description.hashCode()
        return result
    }
}

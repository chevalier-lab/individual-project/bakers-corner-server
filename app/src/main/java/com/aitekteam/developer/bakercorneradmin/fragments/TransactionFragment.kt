package com.aitekteam.developer.bakercorneradmin.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_ORDERS
import com.aitekteam.developer.bakercorneradmin.utils.Helpers
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.transaction.DetailTransactionActivity
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_transaction.*
import kotlinx.android.synthetic.main.item_order.view.*

class TransactionFragment : Fragment() {

    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    val listOrder = mutableListOf<OrderProduct>()
    private lateinit var adapter: AdapterUtil<OrderProduct>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_transaction, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup Title
        (activity as HomeActivity).supportActionBar?.apply {
            this.title = getString(R.string.transaction)
        }

        // init database
        database = FirebaseDatabase.getInstance().getReference(DB_ORDERS)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        progressDialog.show()

        // get data
        database.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    for (orderSnapshot in data.children) {
                        val order = orderSnapshot.getValue(OrderProduct::class.java)
                        order?.let {
                            if (it.is_accept != 3) listOrder.add(it)
                        }
                    }
                }

                // init adapter
                adapter = AdapterUtil(R.layout.item_order,
                    listOrder.sortedBy { o -> o.is_accept }, { itemView, item ->
                        itemView.tv_order_code.text =
                            StringBuilder().append("Order #").append(item.order_code)
                        itemView.tv_qty.text =
                            StringBuilder().append("Qty : ").append(item.total_qty).append(" pcs")
                        itemView.tv_harga.text = Helpers.rupiah(item.total_price.toDouble())
                        itemView.tv_tanggal.text = Helpers.convertLongToDateString(item.time_stamp)

                        // tipe order
                        if (item.type.size == 2) {
                            itemView.tv_type.text = StringBuilder().append("Tipe : Product, Request")
                        } else if (item.type[0] == 0) {
                            itemView.tv_type.text = StringBuilder().append("Tipe : Product")
                        } else {
                            itemView.tv_type.text = StringBuilder().append("Tipe : Request")
                        }

                        // status
                        when (item.is_accept) {
                            0 -> {
                                itemView.tv_status.text = StringBuilder().append("Belum diproses")
                                itemView.tv_status.setTextColor(resources.getColor(R.color.colorGrayLight))
                            }
                            1 -> {
                                itemView.tv_status.text = StringBuilder().append("Diproses")
                                itemView.tv_status.setTextColor(resources.getColor(R.color.colorYellow))
                            }
                            2 -> {
                                itemView.tv_status.text = StringBuilder().append("Ditolak")
                                itemView.tv_status.setTextColor(resources.getColor(R.color.colorRed))
                            }
                            3 -> {
                                itemView.tv_status.text = StringBuilder().append("Sukses")
                                itemView.tv_status.setTextColor(resources.getColor(R.color.colorGreen))
                            }
                        }

                    }, { _, item ->
                        val intent = Intent(context, DetailTransactionActivity::class.java)
                        intent.putExtra("uid", item.uid)
                        startActivity(intent)
                    }
                )

                // init rv
                rv_orders.apply {
                    this.layoutManager = LinearLayoutManager(requireContext())
                    this.adapter = this@TransactionFragment.adapter
                }

                progressDialog.dismiss()
                database.removeEventListener(this)
            }
        })

        database.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(data: DataSnapshot, p1: String?) {
                val order = data.getValue(OrderProduct::class.java)
                listOrder.remove(findOrder(order?.uid))
                order?.let {
                    if (it.is_accept != 3) listOrder.add(it)
                }
                adapter.data = listOrder.sortedBy { o -> o.is_accept }
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

        })
    }

    fun findOrder(uid:String?) : OrderProduct?{
        var orderProduct : OrderProduct? = null;
        for(order:OrderProduct in listOrder){
            if(order.uid == uid) orderProduct = order
        }
        return orderProduct
    }
}

package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class Users(
    var uid: String = "",
    var email: String = "",
    var name: String = "",
    var phone_number: String = "",
    var address: String = "",
    var face: String = "",
    var userType: UserType = UserType(),
    var userUtility: UserUtility = UserUtility()
): Serializable
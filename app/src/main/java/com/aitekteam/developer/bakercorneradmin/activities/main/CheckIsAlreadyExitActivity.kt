package com.aitekteam.developer.bakercorneradmin.activities.main

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.aitekteam.developer.bakercorneradmin.utils.DB_USERS
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.profile.ProfileActivity
import com.aitekteam.developer.bakercorneradmin.models.UserType
import com.aitekteam.developer.bakercorneradmin.models.Users
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*

class CheckIsAlreadyExitActivity : AppCompatActivity() {

    // Declare Variable
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_is_already_exit)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Authentication
        auth = FirebaseAuth.getInstance()

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        progressDialog.show()
    }

    // Visibility Splashscreen or not
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            this.checkAlreadyUser(user)
        } else {
            startActivity(Intent(this@CheckIsAlreadyExitActivity, MainActivity::class.java))
            finish()
            progressDialog.dismiss()
        }
    }

    private fun checkAlreadyUser(user: FirebaseUser) {
        database.child(DB_USERS).child(user.uid).addListenerForSingleValueEvent(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    Log.d(TAG, "" + p0.childrenCount)
                    p0.getValue(Users::class.java)?.apply {
                        if (this.phone_number == "" || this.address == "")
                            startActivity(Intent(this@CheckIsAlreadyExitActivity, ProfileActivity::class.java))
                        else
                            startActivity(Intent(this@CheckIsAlreadyExitActivity, HomeActivity::class.java))
                        finish()
                    }
                    progressDialog.dismiss()
                }
                else {
                    database.child(DB_USERS).child(user.uid).setValue(Users(
                        user.uid,
                        user.email!!,
                        user.displayName!!,
                        "",
                        "",
                        user.photoUrl.toString(),
                        UserType(
                            user.uid,
                            1
                        )
                    )).addOnCompleteListener {
                        startActivity(Intent(this@CheckIsAlreadyExitActivity, ProfileActivity::class.java))
                        finish()
                    }
                    progressDialog.dismiss()
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        // Run Authentication First
        updateUI(auth.currentUser)
    }

    // Local Scope Variable
    companion object {
        const val TAG = "CHECK ALREADY EXIT"
    }

}

package com.aitekteam.developer.bakercorneradmin.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.transaction.ListTransactionActivity
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.aitekteam.developer.bakercorneradmin.models.Users
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_our_users.*
import kotlinx.android.synthetic.main.item_users.view.*

class OurUsersFragment: Fragment() {

    private lateinit var database : FirebaseDatabase
    private lateinit var progressDialog : ProgressDialog
    private lateinit var adapter : AdapterUtil<Users>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_our_users, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup Title
        (activity as HomeActivity).supportActionBar?.apply {
            this.title = getString(R.string.users)
        }

        database = FirebaseDatabase.getInstance()

        progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Mohon Menunggu...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)
        progressDialog.show()

        database.getReference("users").addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) { }

            override fun onDataChange(p0: DataSnapshot) {
                val listUsers = mutableListOf<Users>()
                val listOrder = mutableListOf<OrderProduct>()

                //get data user
                if (p0.exists()){
                    for (UsersSnapshot in p0.children){
                        val users = UsersSnapshot.getValue(Users::class.java)
                        users?.let { listUsers.add(it) }
                    }
                }

                //get data order
                database.getReference("orders").addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {}

                    override fun onDataChange(p0: DataSnapshot) {
                        if (p0.exists()) {
                            for (orderSnapshot in p0.children) {
                                val order = orderSnapshot.getValue(OrderProduct::class.java)
                                order?.let { listOrder.add(it) }
                            }
                        }

                        adapter = AdapterUtil(R.layout.item_users,
                            listUsers.filter { u -> u.userType.type == 0 }, { itemView, item ->
                                var jumlah = 0
                                listOrder.filter { op -> op.is_accept == 3 }.map {
                                    if (it. user.uid == item.uid) jumlah += 1
                                }

                                itemView.tv_nama.text = StringBuilder().append(item.name)
                                itemView.tv_transaksi.text = jumlah.toString()

                                //view image
                                val requestOptions = RequestOptions()
                                Glide.with(requireContext())
                                    .load(item.face)
                                    .apply(requestOptions.circleCrop())
                                    .into(itemView.face)

                            }, { _, item ->
                                val intent = Intent(context, ListTransactionActivity::class.java).apply {
                                    putExtra("user", item)
                                }
                                startActivity(intent)
                            })

                        rv_users.apply {
                            this.layoutManager = LinearLayoutManager(requireContext())
                            this.adapter = this@OurUsersFragment.adapter
                        }

                        progressDialog.dismiss()
                        database.getReference("orders").removeEventListener(this)
                    }

                })
                database.getReference("users").removeEventListener(this)
            }

        })

    }
}

package com.aitekteam.developer.bakercorneradmin.activities.product

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_PRODUCTS
import com.aitekteam.developer.bakercorneradmin.utils.DB_USERS
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.main.MainActivity
import com.aitekteam.developer.bakercorneradmin.models.Product
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.item_product.view.*

class ProductActivity : AppCompatActivity() {

    // Declare Variable
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        // Setup Toolbar
        setupToolbar()

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Authentication
        auth = FirebaseAuth.getInstance()

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        progressDialog.show()

        // Setup List Category Product
        listProduct.layoutManager = LinearLayoutManager(this@ProductActivity)
        database.child(DB_PRODUCTS).addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.childrenCount > 0) {
                    val items: MutableList<Product> = arrayListOf()
                    for (item: DataSnapshot in p0.children) {
                        item.getValue(Product::class.java)?.apply {
                            items.add(this)
                        }
                    }
                    items.sortWith(compareByDescending {it.is_available})

                    listProduct.adapter = AdapterUtil(R.layout.item_product, items,
                        {itemView, item ->
                            val availability = itemView.item_availability
                            val name = itemView.item_name
                            val price = itemView.item_price
                            val category = itemView.item_category

                            Glide.with(applicationContext).load(item.photos)
                                .transform(RoundedCorners(16))
                                .into(itemView.item_cover)

                            availability.text = when(item.is_available) {
                                0 -> "Habis"
                                1 -> "Pre Order"
                                else -> "Ready"
                            }
                            availability.setTextColor(ContextCompat.getColor(applicationContext, when(item.is_available) {
                                0 -> R.color.colorRed
                                1 -> R.color.colorYellow
                                else -> R.color.colorGreen
                            }))
                            name.text = item.name
                            price.text = StringBuilder().append("Rp ").append(item.price)
                            category.text = item.productCategory.category
                        },
                        {_, item ->
                            startActivity(Intent(this@ProductActivity,
                                ProductDetailActivity::class.java).putExtra("itemProduct",item))
                        })
                }
                database.child(DB_PRODUCTS).removeEventListener(this)
            }
        })
    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.product)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    // Visibility Splashscreen or not
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            this.checkAlreadyUser(user)
        } else {
            progressDialog.dismiss()
            startActivity(Intent(this@ProductActivity, MainActivity::class.java))
            finish()
        }
    }

    // Check Already User Profile
    private fun checkAlreadyUser(user: FirebaseUser) {
        database.child(DB_USERS).child(user.uid).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (!p0.exists()) {
                    progressDialog.dismiss()
                    startActivity(Intent(this@ProductActivity, MainActivity::class.java))
                    finish()
                }
                else progressDialog.dismiss()
                database.child(DB_USERS).child(user.uid).removeEventListener(this)
            }
        })
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@ProductActivity, HomeActivity::class.java))
        finish()
    }

    override fun onStart() {
        super.onStart()
        // Run Authentication First
        updateUI(auth.currentUser)
    }

    // Local Scope Variable
    companion object {
        const val TAG = "PRODUCT"
    }
}

package com.aitekteam.developer.bakercorneradmin.activities.profile

import android.app.ProgressDialog
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_ORDERS
import com.aitekteam.developer.bakercorneradmin.utils.Helpers
import com.aitekteam.developer.bakercorneradmin.utils.Helpers.convertProductToDetailRiwayat
import com.aitekteam.developer.bakercorneradmin.utils.Helpers.convertRequestToDetailRiwayat
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.aitekteam.developer.bakercorneradmin.models.items.DetailRiwayatTransaksiItem
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_detail_riwayat_transaksi.*
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.item_detail_riwayat.view.*

class DetailRiwayatTransaksiActivity : AppCompatActivity() {

    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    private lateinit var adapter: AdapterUtil<DetailRiwayatTransaksiItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_riwayat_transaksi)
        setupToolbar()

        // init database
        database = FirebaseDatabase.getInstance().getReference(DB_ORDERS)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this@DetailRiwayatTransaksiActivity)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        progressDialog.show()

        //get parceble data order
        val orderProduct: OrderProduct = intent.getSerializableExtra("dataOrder")!! as OrderProduct

        //init UI
        initUI(orderProduct)


        // get data
        database.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(data: DataSnapshot) {
                val listOrder = mutableListOf<OrderProduct>()
                if (data.exists()) {
                    for (orderSnapshot in data.children) {
                        val order = orderSnapshot.getValue(OrderProduct::class.java)
                        order?.let { listOrder.add(orderProduct) }
                    }
                }

                val listDataDetail = mutableListOf<DetailRiwayatTransaksiItem>()
                //add req to list data
                orderProduct.productRequests.map {
                    listDataDetail.add(it.convertRequestToDetailRiwayat())
                }

                //add product to list data
                orderProduct.products.map {
                    listDataDetail.add(it.convertProductToDetailRiwayat())
                }

                // init adapter
                adapter = AdapterUtil(R.layout.item_detail_riwayat,
                    listDataDetail.sortedBy { o -> o.is_accept },
                    { itemView,item ->
                        itemView.tv_namapesanan.text= StringBuilder().append(item.name)
                        //set type
                        if (item.type == 0){
                            itemView.tv_type.text=getString(R.string.show_tipe, "Product")
                        } else {
                            itemView.tv_type.text = getString(R.string.show_tipe, "Request")
                        }

                        //set qty
                        itemView.tv_qty.text= getString(R.string.show_qty_2, item.qty.toString())

                        // set total harga
                        itemView.tv_harga.text = Helpers.rupiah(item.price.toDouble() * item.qty.toDouble())
                    },
                    { _, _ -> })

                // init rv
                rv_detailriwayattransaksi.apply {
                    this.layoutManager = LinearLayoutManager(context)
                    this.adapter = this@DetailRiwayatTransaksiActivity.adapter
                }
                progressDialog.dismiss()
                database.removeEventListener(this)
            }
        })
    }

    // Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = getString(R.string.transaction)
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }

    //init ui
    private fun initUI(orderProduct: OrderProduct){
        //set title
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = StringBuilder().append("Order #",orderProduct.order_code)
            tv_total.text= Helpers.rupiah(orderProduct.total_price.toDouble())
            this.setDisplayHomeAsUpEnabled(true)
        }
    }
}

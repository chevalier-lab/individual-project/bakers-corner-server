package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class UserUtility (
    var uid: String = "",
    var transaction: Long = 0,
    var balance: Long = 0
): Serializable
package com.aitekteam.developer.bakercorneradmin.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_ORDERS
import com.aitekteam.developer.bakercorneradmin.utils.DB_USERS
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.main.HomeActivity
import com.aitekteam.developer.bakercorneradmin.activities.main.MainActivity
import com.aitekteam.developer.bakercorneradmin.activities.product.CategoryProductActivity
import com.aitekteam.developer.bakercorneradmin.activities.product.CreateCategoryProductActivity
import com.aitekteam.developer.bakercorneradmin.activities.product.CreateProductActivity
import com.aitekteam.developer.bakercorneradmin.activities.product.ProductActivity
import com.aitekteam.developer.bakercorneradmin.activities.profile.ProfileActivity
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.aitekteam.developer.bakercorneradmin.models.Users
import com.aitekteam.developer.bakercorneradmin.models.items.HomeMenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.item_menu_home.view.*
import java.lang.StringBuilder

class HomeFragment: Fragment() {

    // Declare Variable
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    private var jumlahTransaksi = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_home, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup SupportActionBar title
        (activity as HomeActivity).supportActionBar?.apply {
            this.title = getString(R.string.bakers_corner)
        }

        // Set Option Menu
        setHasOptionsMenu(true)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Authentication
        auth = FirebaseAuth.getInstance()

        // Setup Database
        database = FirebaseDatabase.getInstance().reference

        userInfoTrx?.text = StringBuilder().append(jumlahTransaksi).append(" transaksi")

        progressDialog.show()

        // Setup Home Menu
        listHomeMenu.layoutManager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        listHomeMenu.adapter = AdapterUtil(R.layout.item_menu_home,
            listOf(
                HomeMenuItem("Buat \nProduk"),
                HomeMenuItem("Buat \nKategori"),
                HomeMenuItem("Daftar \nProduk"),
                HomeMenuItem("Daftar \nKategori")
            ), { itemView, item ->
//                val label = itemView.findViewById<TextView>(R.id.item_label)
//                label.text = item.label
                itemView.item_label.text = item.label
                itemView.item_card.setOnClickListener {
                    when (itemView.item_label.text) {
                        "Buat \nProduk" -> startActivity(Intent(context, CreateProductActivity::class.java))
                        "Buat \nKategori" -> startActivity(Intent(context, CreateCategoryProductActivity::class.java))
                        "Daftar \nProduk" -> startActivity(Intent(context, ProductActivity::class.java))
                        "Daftar \nKategori" -> startActivity(Intent(context, CategoryProductActivity::class.java))
                    }
                    activity!!.finish()
                }

                // set icon
                when (itemView.item_label.text) {
                    "Daftar \nProduk" -> itemView.image_label.setImageResource(R.drawable.ic_list)
                    "Daftar \nKategori" -> itemView.image_label.setImageResource(R.drawable.ic_list)
                }
            }, { _, _ ->
//                when (position) {
//                    0 -> startActivity(Intent(context, CreateProductActivity::class.java))
//                    1 -> startActivity(Intent(context, CreateCategoryProductActivity::class.java))
//                    2 -> startActivity(Intent(context, ProductActivity::class.java))
//                    else -> startActivity(Intent(context, CategoryProductActivity::class.java))
//                }
//                activity!!.finish()
            })

    }

    // Visibility Splashscreen or not
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            this.checkAlreadyUser(user)
        } else {
            progressDialog.dismiss()
            startActivity(Intent(context, MainActivity::class.java))
            activity!!.finish()
        }
    }

    // Check Already User Profile
    private fun checkAlreadyUser(user: FirebaseUser) {
        database.child(DB_USERS).child(user.uid).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    Log.d(TAG, "" + p0.childrenCount)
                    p0.getValue(Users::class.java)?.apply {setupProfile(this)}
                }
                else {
                    progressDialog.dismiss()
                    startActivity(Intent(context, MainActivity::class.java))
                    activity!!.finish()
                }
                database.child(DB_USERS).child(user.uid).removeEventListener(this)
            }
        })
    }

    // Plot Profile
    private fun setupProfile(user: Users) {
        // Setup User Info Face
        val requestOptions = RequestOptions()
        Glide.with(activity!!.applicationContext).load(user.face)
            .apply(requestOptions.circleCrop())
            .into(userInfoFace)
        // Setup User Info Name
        userInfoName.text = user.name
        // Setup User Transaction
//        userInfoTrx.text = StringBuilder().append(user.userUtility.transaction).append(" Transaksi")

        // setup jumlah transaksi
        database.child(DB_ORDERS).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) { }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
//                    var jumlahTransaksi = 0
                    for(transaksi in p0.children) {
                        val data = transaksi.getValue(OrderProduct::class.java)
                        if (data?.is_accept == 3) {
                            jumlahTransaksi += 1
                        }
                    }

                    userInfoTrx?.text = StringBuilder().append(jumlahTransaksi).append(" transaksi")
                    jumlahTransaksi = 0
                }
                database.child(DB_ORDERS).removeEventListener(this)
            }

        })
        // Setup User Profit
//        userInfoProfit.text = StringBuilder().append("Rp ").append(user.userUtility.balance)
        progressDialog.dismiss()
    }

    override fun onStart() {
        super.onStart()
        // Run Authentication First
        updateUI(auth.currentUser)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.action_menu_user, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    // Handler Toolbar Icon Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.item_profile -> {
            startActivity(Intent(context, ProfileActivity::class.java))
            activity!!.finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    // Local Scope Variable
    companion object {
        const val TAG = "HOME"
    }
}
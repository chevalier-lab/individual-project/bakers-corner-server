package com.aitekteam.developer.bakercorneradmin.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.activities.transaction.DetailTransactionActivity
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.google.gson.Gson
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage

class NotificationService : Service() {

    private var mqtt: MqttService? = null
    private val TAG = "BC-MyService"
    private val orderChannelId = "order"
    private val channelName = "Notification Service"

    override fun onCreate() {
        super.onCreate()
        // create notification
        val manager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("foreground", "Foreground Service (you can hide it)", NotificationManager.IMPORTANCE_NONE)
            manager.createNotificationChannel(channel)
        }

        // create builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, "foreground")
            .setContentText("Aplikasi sedang berjalan, anda bisa menyembunyikan ini di pengaturan")
            .setSmallIcon(R.mipmap.logo_bakers)
            .setAutoCancel(true)
            .setNotificationSilent()
            .build()

        startForeground(3, notificationBuilder)
    }

    private fun showMessage(message: String) {
        Log.i(TAG, message)
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        mqttService()
        showMessage("Service started")

        return START_STICKY
    }

    private fun mqttService() {
        mqtt = MqttService(applicationContext)
        mqtt!!.setCallback(object : MqttCallbackExtended {
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                showMessage(message.toString())
                val data = Gson().fromJson<OrderProduct>(message.toString(), OrderProduct::class.java)
                displayNotification("Order #${data.order_code}", data)
            }

            override fun connectComplete(reconnect: Boolean, serverURI: String?) { }
            override fun connectionLost(cause: Throwable?) { }
            override fun deliveryComplete(token: IMqttDeliveryToken?) { }

        })
    }

    private fun displayNotification(content: String, dataOrderProduct: OrderProduct) {
        val manager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(orderChannelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }

        // pending intent
        val intent = Intent(applicationContext, DetailTransactionActivity::class.java).apply {
            this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.putExtra("uid", dataOrderProduct.uid)
        }
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // create builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, orderChannelId)
            .setContentTitle("Ada pesanan masuk nih!!")
            .setContentText(content)
            .setSmallIcon(R.mipmap.logo_bakers)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        manager.notify(1,notificationBuilder)
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        showMessage("Task ended, send broadcast")
        val broadcastIntent = Intent()
        broadcastIntent.action = "StartService"
        broadcastIntent.setClass(this, ReceiverService::class.java)
        sendBroadcast(broadcastIntent)
        super.onTaskRemoved(rootIntent)
    }
}
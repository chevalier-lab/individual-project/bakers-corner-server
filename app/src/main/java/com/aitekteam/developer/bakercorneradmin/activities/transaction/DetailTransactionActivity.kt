package com.aitekteam.developer.bakercorneradmin.activities.transaction

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_ORDERS
import com.aitekteam.developer.bakercorneradmin.utils.DialogUtil
import com.aitekteam.developer.bakercorneradmin.utils.Helpers.convertProductToDetailRiwayat
import com.aitekteam.developer.bakercorneradmin.utils.Helpers.convertRequestToDetailRiwayat
import com.aitekteam.developer.bakercorneradmin.utils.Helpers.rupiah
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.aitekteam.developer.bakercorneradmin.models.ProductRequest
import com.aitekteam.developer.bakercorneradmin.models.items.DetailRiwayatTransaksiItem
import com.aitekteam.developer.bakercorneradmin.services.MqttService
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail_transaction.*
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.item_product_transaction.view.*
import org.eclipse.paho.client.mqttv3.MqttMessage
import java.net.URLEncoder

@Suppress("SpellCheckingInspection")
class DetailTransactionActivity : AppCompatActivity() {
    private lateinit var orderRef: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    private lateinit var adapter: AdapterUtil<DetailRiwayatTransaksiItem>
    val listData = mutableListOf<DetailRiwayatTransaksiItem>()
    private var mqtt: MqttService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_transaction)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // init mqtt
        mqtt = MqttService(this)

        progressDialog.show()
        setOrderProduct()
    }

    private fun setOrderProduct() {
        val uid = intent.getStringExtra("uid")

        orderRef = FirebaseDatabase.getInstance().getReference(DB_ORDERS).child(uid!!)

        orderRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    listData.clear()

                    val order = data.getValue(OrderProduct::class.java)

                    if (order != null) {
                        var totalPrice: Long = 0

                        order.productRequests.map {
                            listData.add(it.convertRequestToDetailRiwayat())
                            if ((it.is_accept == 0).or(it.is_accept == 1))
                                totalPrice += it.qty * it.price
                        }

                        order.products.map {
                            listData.add(it.convertProductToDetailRiwayat())
                            totalPrice += it.qty * it.price
                        }

                        setupToolbar(order)
                    }

                    adapter = AdapterUtil(R.layout.item_product_transaction, listData,
                        { itemView, item ->
                            itemView.tv_product_name.text = item.name

                            if (item.type == 0) itemView.tv_product_type.text =
                                getString(R.string.show_tipe, "Product")
                            else itemView.tv_product_type.text =
                                getString(R.string.show_tipe, "Request")

                            itemView.tv_product_price.text = rupiah(item.price.toDouble())
                            itemView.tv_product_qty.text = StringBuilder().apply {
                                append("Qty : ").append(item.qty).append(" pcs")
                            }

                            if (item.requestUid == "") {
                                itemView.view_request.visibility = View.GONE
                                itemView.tv_status.visibility = View.GONE
                            } else {
                                when (item.is_accept) {
                                    0 -> {
                                        itemView.tv_status.text =
                                            StringBuilder().append("Belum diproses")
                                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorGrayLight))
                                    }
                                    1 -> {
                                        itemView.tv_status.text = StringBuilder().append("Diproses")
                                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorYellow))
                                    }
                                    2 -> {
                                        itemView.tv_status.text = StringBuilder().append("Ditolak")
                                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorRed))
                                    }
                                    3 -> {
                                        itemView.tv_status.text = StringBuilder().append("Sukses")
                                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorGreen))
                                    }
                                }

                            }
                        },
                        { index, item ->
                            if (item.requestUid != "") {
                                val intent = Intent(baseContext, ProductRequestActivity::class.java)
                                intent.putExtra("uid", order?.uid)
                                intent.putExtra("RequestIndex", index)
                                startActivity(intent)
                            }
                        }
                    )

                    rv_product?.apply {
                        this.layoutManager = LinearLayoutManager(context)
                        this.adapter = this@DetailTransactionActivity.adapter
                    }

                    progressDialog.dismiss()
                    orderRef.removeEventListener(this)
                }
            }
        })

        orderRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                listData.clear()
                val order = p0.getValue(OrderProduct::class.java)

                if (order != null) {
                    var totalPrice: Long = 0

                    order.productRequests.map {
                        listData.add(it.convertRequestToDetailRiwayat())
                        if ((it.is_accept == 0).or(it.is_accept == 1))
                            totalPrice += it.qty * it.price
                    }

                    order.products.map {
                        listData.add(it.convertProductToDetailRiwayat())
                        totalPrice += it.qty * it.price
                    }
                    setupToolbar(order)
                }
                adapter.data = listData

                if (order != null) setButton(order)
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar(order: OrderProduct) {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = StringBuilder().append("Order #").append(order.order_code)
            tv_price.text = rupiah(order.total_price.toDouble())
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onBackPressed() {
        finish()
    }

    private fun requestCheckDialog() {
        DialogUtil.showDialogPossitiveOnly(
            this,
            "Peringatan",
            "Harap selesaikan produk request terlebih dahulu!"
        )
    }

    private fun checkRequest(request: List<ProductRequest>): Boolean {
        var bool = false
        for (productRequest: ProductRequest in request) {
            if (productRequest.is_accept >= 2) bool = true
        }
        return bool
    }

    private fun setButton(order: OrderProduct) {
        val phoneNumber = order.user.phone_number
        val countryCodedPhone = "+62${phoneNumber.substring(1, phoneNumber.length)}"

        btn_contact.setOnClickListener {
            contactBuyer(
                countryCodedPhone,
                "Hallo kak ${order.user.name}, saya dari " +
                        "pihak Bakers Corner ingin konfirmasi pesanan dengan nomor " +
                        "pesanan ${order.order_code} yang kakak pesan di aplikasi kami, terima kasih"
            )
        }

        when (order.is_accept) {
            0 -> {
                btn_process1.apply {
                    setBackgroundColor(Color.parseColor("#EC8D01"))
                    text = getString(R.string.proses)
                }

                btn_process1.setOnClickListener {
                    if (checkRequest(order.productRequests) || order.productRequests.isEmpty()) {
                        DialogUtil.showDialog(
                            this,
                            "Konfirmasi",
                            "Yakin ingin proses pesanan ini?") {
                            orderRef.child("_accept").setValue(1).addOnCompleteListener {
                                if (it.isSuccessful) {
                                    // send notif to client
                                    val dataNotif = Gson().toJson(order)
                                    mqtt?.publish("Cheva/BC/OrderProcess/${order.order_code}", MqttMessage(dataNotif.toString().toByteArray()))
                                }
                            }
                        }
                    } else requestCheckDialog()
                }

                btn_process2.apply {
                    setBackgroundColor(Color.parseColor("#FF2F1F"))
                    text = getString(R.string.tolak)
                }

                btn_process2.setOnClickListener {
                    DialogUtil.showDialog(
                        this,
                        "Konfirmasi",
                        "Yakin ingin menolak pesanan ini?") {
                        orderRef.child("_accept").setValue(2).addOnCompleteListener {
                            if (it.isSuccessful) {
                                // send notif to client
                                val dataNotif = Gson().toJson(order)
                                mqtt?.publish("Cheva/BC/OrderCancel/${order.order_code}", MqttMessage(dataNotif.toString().toByteArray()))
                            }
                        }
                    }
                }

                btn_process1.visibility = View.VISIBLE
                btn_process2.visibility = View.VISIBLE
            }
            1 -> {
                btn_process1.apply {
                    setBackgroundColor(Color.parseColor("#5DDA22"))
                    text = getString(R.string.selesai)
                }

                btn_process1.setOnClickListener {
                    if (checkRequest(order.productRequests) || order.productRequests.isEmpty()) {
                        DialogUtil.showDialog(
                            this,
                            "Konfirmasi",
                            "Yakin ingin selesaikan pesanan ini?") {
                            orderRef.child("_accept").setValue(3).addOnCompleteListener {
                                if (it.isSuccessful) {
                                    // send notif to client
                                    val dataNotif = Gson().toJson(order)
                                    mqtt?.publish("Cheva/BC/OrderSuccess/${order.order_code}", MqttMessage(dataNotif.toString().toByteArray()))
                                }
                            }
                        }

                    } else requestCheckDialog()
                }

                btn_process1.visibility = View.VISIBLE
                btn_process2.visibility = View.GONE
            }
            2 -> {
                btn_process1.visibility = View.GONE
                btn_process2.visibility = View.GONE
            }
            3 -> {
                btn_process1.visibility = View.GONE
                btn_process2.visibility = View.GONE
            }
        }
    }
    // Contact buyer using Whatsapp
    private fun contactBuyer(phoneNumber: String, message: String) {
        try {
            val pm = packageManager
            val url = "https://api.whatsapp.com/send?phone=$phoneNumber&text=${
            URLEncoder.encode(
                message,
                "UTF-8"
            )}"

            val intent = Intent(Intent.ACTION_VIEW).apply {
                type = "text/plain"
                setPackage("com.whatsapp")
                data = Uri.parse(url)
            }

            if (intent.resolveActivity(pm) != null) startActivity(intent)
            else {
                Snackbar.make(
                    btn_contact,
                    "Failed to open Whatsapp!",
                    Snackbar.LENGTH_SHORT
                ).show()
            }

        } catch (e: Exception) {
            Snackbar.make(
                btn_contact,
                "Failed to open Whatsapp!",
                Snackbar.LENGTH_SHORT
            ).show()

            Log.i(
                "DetailTransaction",
                "Open WA Failed! ${e.message} --- ${e.printStackTrace()}"
            )
        }
    }

}

package com.aitekteam.developer.bakercorneradmin.models

import java.io.Serializable

data class UserType (
    var uid: String = "",
    var type: Int = 0
): Serializable
package com.aitekteam.developer.bakercorneradmin

import android.app.Application
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.aitekteam.developer.bakercorneradmin.services.NotificationWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class BakersCornerApplication : Application() {

    private val applicationScope = CoroutineScope(Dispatchers.Default)

    override fun onCreate() {
        super.onCreate()
        applicationScope.launch {
            setupWorker()
        }
    }

    private fun setupWorker() {
        // create periodic work
        val task = OneTimeWorkRequest.Builder(
            NotificationWorker::class.java
        ).build()
        val workManager = WorkManager.getInstance()
        workManager.enqueue(task)
    }
}
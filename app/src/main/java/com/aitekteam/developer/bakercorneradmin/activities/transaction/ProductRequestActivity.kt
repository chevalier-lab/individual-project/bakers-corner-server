package com.aitekteam.developer.bakercorneradmin.activities.transaction

import android.app.ProgressDialog
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.aitekteam.developer.bakercorneradmin.utils.AdapterUtil
import com.aitekteam.developer.bakercorneradmin.utils.DB_ORDERS
import com.aitekteam.developer.bakercorneradmin.utils.DialogUtil
import com.aitekteam.developer.bakercorneradmin.utils.Helpers.convertRequestToDetailRiwayat
import com.aitekteam.developer.bakercorneradmin.utils.Helpers.rupiah
import com.aitekteam.developer.bakercorneradmin.R
import com.aitekteam.developer.bakercorneradmin.models.OrderProduct
import com.aitekteam.developer.bakercorneradmin.models.items.DetailRiwayatTransaksiItem
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_detail_transaction.*
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.item_product_transaction.view.*

class ProductRequestActivity : AppCompatActivity() {
    private lateinit var orderRef: DatabaseReference
    private lateinit var progressDialog: ProgressDialog
    private lateinit var adapter: AdapterUtil<DetailRiwayatTransaksiItem>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_transaction)

        // Setup Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        progressDialog.show()
        setOrderProduct()
    }

    private fun setOrderProduct() {
        val uid = intent.getStringExtra("uid")
        val index = intent.getIntExtra("RequestIndex",0)
        orderRef = FirebaseDatabase.getInstance().getReference(DB_ORDERS).child(uid!!)
        orderRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(data: DataSnapshot) {
                val listData = mutableListOf<DetailRiwayatTransaksiItem>()
                if (data.exists()) {
                    val order = data.getValue(OrderProduct::class.java)
                    val productRequest = order!!.productRequests.get(index)
                    var totalPrice : Long = productRequest.price * productRequest.qty
                    setupToolbar(order.productRequests.get(index).uid,totalPrice)
                    listData.add(productRequest.convertRequestToDetailRiwayat())

                    adapter = AdapterUtil(R.layout.item_product_transaction,
                        listData,
                        { itemView, item ->
                            itemView.tv_product_name.text = item.name
                            if (item.type == 0) {
                                itemView.tv_product_type.text =
                                    getString(R.string.show_tipe, "Product")
                            } else {
                                itemView.tv_product_type.text =
                                    getString(R.string.show_tipe, "Request")
                            }

                            itemView.tv_product_price.text = rupiah(item.price.toDouble())
                            itemView.tv_product_qty.text =
                                StringBuilder().append("Qty : ").append(item.qty).append(" pcs")

                            if (item.requestUid == "") {
                                itemView.view_request.visibility = View.GONE
                                itemView.tv_status.visibility = View.GONE
                            } else {
                                when (item.is_accept) {
                                    0 -> itemView.tv_status.text =
                                        java.lang.StringBuilder().append("Belum diproses")
                                    1 -> {
                                        itemView.tv_status.text =
                                            java.lang.StringBuilder().append("Diproses")
                                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorYellow))
                                    }
                                    2 -> {
                                        itemView.tv_status.text =
                                            java.lang.StringBuilder().append("Ditolak")
                                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorRed))
                                    }
                                    3 -> {
                                        itemView.tv_status.text =
                                            java.lang.StringBuilder().append("Sukses")
                                        itemView.tv_status.setTextColor(resources.getColor(R.color.colorGreen))
                                    }
                                }

                            }
                        },
                        { _, _ -> }
                    )

                    rv_product.apply {
                        this.layoutManager = LinearLayoutManager(context)
                        this.adapter = this@ProductRequestActivity.adapter
                    }

                    when (productRequest.is_accept) {
                        0 -> {
                            // visible button process and reject
                            btn_process1.visibility = View.VISIBLE
                            btn_process2.visibility = View.VISIBLE

                            // button process
                            btn_process1.setBackgroundColor(Color.parseColor("#EC8D01"))
                            btn_process1.text = getString(R.string.proses)
                            btn_process1.setOnClickListener {
                                DialogUtil.showDialog(
                                    this@ProductRequestActivity,
                                    "Konfirmasi",
                                    "Yakin ingin proses request ini?") {
                                    orderRef.child("productRequests")
                                        .child(index.toString())
                                        .child("_accept")
                                        .setValue(1)
                                    finish()
                                }
                            }

                            // button reject
                            btn_process2.setBackgroundColor(Color.parseColor("#FF2F1F"))
                            btn_process2.text = getString(R.string.tolak)
                            btn_process2.setOnClickListener {
                                DialogUtil.showDialog(
                                    this@ProductRequestActivity,
                                    "Konfirmasi",
                                    "Yakin ingin menolak request ini?") {
                                    orderRef.child("productRequests")
                                        .child(index.toString())
                                        .child("_accept")
                                        .setValue(2)
                                    finish()
                                }
                            }
                        }
                        1 -> {
                            // hide button reject
                            btn_process1.visibility = View.VISIBLE
                            btn_process2.visibility = View.GONE

                            btn_process1.setBackgroundColor(Color.parseColor("#5DDA22"))
                            btn_process1.text = getString(R.string.selesai)
                            btn_process1.setOnClickListener {
                                DialogUtil.showDialog(
                                    this@ProductRequestActivity,
                                    "Konfirmasi",
                                    "Yakin ingin selesaikan request ini?") {
                                    orderRef.child("productRequests")
                                        .child(index.toString())
                                        .child("_accept")
                                        .setValue(3)
                                    finish()
                                }
                            }
                        }
                        2 -> {
                            btn_process1.visibility = View.GONE
                            btn_process2.visibility = View.GONE
                        }
                        3 -> {
                            btn_process1.visibility = View.GONE
                            btn_process2.visibility = View.GONE
                        }

                    }
                    progressDialog.dismiss()
                    orderRef.removeEventListener(this)
                }
            }

        })
    }


    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar(uid: String,price : Long) {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            this.title = StringBuilder().append("Request #").append(uid)
            tv_price.text = rupiah(price.toDouble())
            this.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onBackPressed() {
        finish()
    }
}

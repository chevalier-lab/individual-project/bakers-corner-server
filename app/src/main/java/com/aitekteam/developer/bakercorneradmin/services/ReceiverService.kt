package com.aitekteam.developer.bakercorneradmin.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log

class ReceiverService : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i("BC-MyService", "Broadcast diterima")
        // start service
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context?.startForegroundService(Intent(context, NotificationService::class.java))
        } else {
            context?.startService(Intent(context, NotificationService::class.java))
        }
    }
}
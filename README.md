# Bakers Corner Server
Merupakan aplikasi pengelolaan dan pemesanan dari Bakers Corner Fakultas Ilmu Terapan.

## Deskripsi Fitur
Aplikasi ini dibangun dengan fitur sebagai berikut:
1. Sign In
2. Menus
3. Filter Menu
4. Request Menu
5. Order

## Deskripsi Teknologi
Aplikasi ini dibangun dengan mengggunakan:
1. Android Studio 3.6
2. Firebase Realtime Database

## Copyright
Aplikasi ini dikhususkan untuk bakers corner, jika terdapat duplikasi dan publikasi yang tidak melalui izin bakers corner ataupun chevalier lab, maka akan dikenakan sanksi hukum.
